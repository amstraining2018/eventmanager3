package tools;

import static org.junit.Assert.*;

import java.io.IOException;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

//
public class UserFileCheckTest {
	String ok = "c:\\work\\account_20180601.csv";
	String CATCH = "c:\\work\\catch.csv";

	@Test
	public void UserFileCheck正常() throws IOException {
		boolean expect = true;
		boolean ret = UserFileCheck.check(ok);
		assertThat(ret, CoreMatchers.is(expect));
	}

	@Test
	public void UserFileCheck異常1個目がSかどうか() throws IOException {
		boolean expect = false;
		boolean ret = UserFileCheck.check("c:\\work\\startColumn.csv");
		assertThat(ret, CoreMatchers.is(expect));
	}

	@Test
	public void UserFileCheck異常ヘッダ行項目の数が正しいかどうか() throws IOException {
		boolean expect = false;
		boolean ret = UserFileCheck.check("c:\\work\\userHeadItem.csv");
		assertThat(ret, CoreMatchers.is(expect));
	}

	@Test
	public void UserFileCheck異常ヘッダ行が２行目であるかどうか() throws IOException {
		boolean expect = false;
		boolean ret = UserFileCheck.check("c:\\work\\userHeadItem2.csv");
		assertThat(ret, CoreMatchers.is(expect));
	}

	@Test
	public void UserFileCheck異常データ行項目の数が正しいかどうか() throws IOException {
		boolean expect = false;
		boolean ret = UserFileCheck.check("c:\\work\\dataItem.csv");
		assertThat(ret, CoreMatchers.is(expect));
	}

	@Test
	public void UserFileCheck異常データの行件数とEに記述された数が正しいかどうか() throws IOException {
		boolean expect = false;
		boolean ret = UserFileCheck.check("c:\\work\\dataRowNum.csv");
		assertThat(ret, CoreMatchers.is(expect));
	}

	@Test
	public void UserFileCheck異常Eが最後の行であるか() throws IOException {
		boolean expect = false;
		boolean ret = UserFileCheck.check("c:\\work\\dataRowNum2.csv");
		assertThat(ret, CoreMatchers.is(expect));
	}

	@Test
	public void UserFileCheck異常SHDE以外がないか() throws IOException {
		boolean expect = false;
		boolean ret = UserFileCheck.check("c:\\work\\chSHDEDig1.csv");
		assertThat(ret, CoreMatchers.is(expect));
	}

	@Test
	public void UserFileCheck異常SHDE以外がないか2() throws IOException {
		boolean expect = false;
		boolean ret = UserFileCheck.check("c:\\work\\columnSHDE.csv");
		assertThat(ret, CoreMatchers.is(expect));
	}

	@Test
	public void UserFileCheck異常_OutOfBoundsException() throws IOException {
		boolean expect = false;
		boolean ret = UserFileCheck.check("c:\\work\\account_20180601_abnormal_bounds.csv");
		assertThat(ret, CoreMatchers.is(expect));
	}

	@Test
	public void UserFileCheck異常() throws IOException {
		boolean expect = false;
		boolean ret = UserFileCheck.check("c:\\work\\err101.csv");
		assertThat(ret, CoreMatchers.is(expect));
	}

}
