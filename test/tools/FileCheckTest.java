package tools;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class FileCheckTest {

	//正常
	private final static String PATH0 = "c:\\work\\room_20180601.csv";
	//異常
	private final static String PATH1 = "c:\\work\\room_20180601_startColumn.csv";
	private final static String PATH2 = "c:\\work\\room_20180601_headItem項目数.csv";
	private final static String PATH3 = "c:\\work\\room_20180601_headItem2行目.csv";
	private final static String PATH4 = "c:\\work\\room_20180601_dataItem.csv";
	private final static String PATH5 = "c:\\work\\room_20180601_dataRowNum件数一致.csv";
	private final static String PATH6 = "c:\\work\\room_20180601_dataRowNum最後E.csv";
	private final static String PATH7 = "c:\\work\\room_20180601_columnSHDE.csv";
	private final static String PATH8 = "c:\\work\\room_20180601_abnormal_bounds.csv";
	private final static String PATH9 = "c:\\work\\room_20180601_NumberFormat.csv";

	@Test
	public void testCheck正常なファイル読み込みを検証() throws IOException {
		//		boolean expect = true; 期待値
		boolean ret = FileCheck.check(PATH0); //実測値
		assertTrue(ret);
	}

	@Test
	public void testCheck異常なファイル読み込みを検証startColumn() throws IOException {
		//		boolean expect = false; 期待値
		boolean ret = FileCheck.check(PATH1); //実測値
		assertFalse(ret);
	}

	@Test
	public void testCheck異常なファイル読み込みを検証headItem項目数() throws IOException {
		//		boolean expect = false; 期待値
		boolean ret = FileCheck.check(PATH2); //実測値
		assertFalse(ret);
	}

	@Test
	public void testCheck異常なファイル読み込みを検証headItem2行目() throws IOException {
		//		boolean expect = false; 期待値
		boolean ret = FileCheck.check(PATH3); //実測値
		assertFalse(ret);
	}

	@Test
	public void testCheck異常なファイル読み込みを検証dataItem() throws IOException {
		//		boolean expect = false; 期待値
		boolean ret = FileCheck.check(PATH4); //実測値
		assertFalse(ret);
	}

	@Test
	public void testCheck異常なファイル読み込みを検証dataRowNum件数一致() throws IOException {
		//		boolean expect = false; 期待値
		boolean ret = FileCheck.check(PATH5); //実測値
		assertFalse(ret);
	}

	@Test
	public void testCheck異常なファイル読み込みを検証dataRowNum最後E() throws IOException {
		//		boolean expect = false; 期待値
		boolean ret = FileCheck.check(PATH6); //実測値
		assertFalse(ret);
	}

	@Test
	public void testCheck異常なファイル読み込みを検証columnSHDE() throws IOException {
		//		boolean expect = false; 期待値
		boolean ret = FileCheck.check(PATH7); //実測値
		assertFalse(ret);
	}

	@Test
	public void testCheck異常なファイル読み込みを検証abnomal_bounds() throws IOException {
		//		boolean expect = false; 期待値
		boolean ret = FileCheck.check(PATH8); //実測値
		assertFalse(ret);
	}

	@Test
	public void testCheck異常なファイル読み込みを検証NumberFormat() throws IOException {
		//		boolean expect = false; 期待値
		boolean ret = FileCheck.check(PATH9); //実測値
		assertFalse(ret);
	}
}
