package tools;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class RecordCheckTest {

	//正常系で参照されるファイル
	String normal = "C:\\work\\room_20180601.csv";

	//異常系で参照されるファイル
	//異常系  nameに中身が存在しない
	String abnormal1 = "C:\\work\\room_20180601_abnormal_nameNull.csv";
	//異常系 nameが100字以上でない
	String abnormal2 = "C:\\work\\room_20180601_abnormal_name100over.csv";
	//異常系 capacityが空文字でない
	String abnormal3 = "C:\\work\\room_20180601_abnormal_capacityNull.csv";
	//異常系 capacityが半角数字でない
	String abnormal4 = "C:\\work\\room_20180601_abnormal_capacityNumber.csv";
	//異常系 micが半角数字でない
	String abnormal5 = "C:\\work\\room_20180601_abnormal_micNumber.csv";
	//異常系 boardが半角数字でない
	String abnormal6 = "C:\\work\\room_20180601_abnormal_boardNumber.csv";
	//異常系 projectorが半角数字でない
	String abnormal7 = "C:\\work\\room_20180601_abnormal_projectorNumber.csv";
	//異常系 closeがnullでない
	String abnormal8 = "C:\\work\\room_20180601_abnormal_closeNull.csv";
	//異常系 closeが正しい形式でない
	String abnormal9 = "C:\\work\\room_20180601_abnormal_closeFormat.csv";

	@Test
	public void recordCheck正常系_BufferedReader() {

		try {
			//期待値を設定(メソッドのreturnで返してほしい値)
			boolean expected = true;

			//実測値
			boolean ret = RecordCheck.check(normal);

			//検証
			assertThat(ret, is(expected));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void recordCheck異常系_BufferedReader_nameNull() {

		try {
			//期待値を設定(メソッドのreturnで返してほしい値)
			boolean expected = false;

			//実測値
			boolean ret = RecordCheck.check(abnormal1);

			//検証
			assertThat(ret, is(expected));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void recordCheck異常系_BufferedReader_name100over() {

		try {
			//期待値を設定(メソッドのreturnで返してほしい値)
			boolean expected = false;

			//実測値
			boolean ret = RecordCheck.check(abnormal2);

			//検証
			assertThat(ret, is(expected));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void recordCheck異常系_BufferedReader_capacityNull() {

		try {
			//期待値を設定(メソッドのreturnで返してほしい値)
			boolean expected = false;

			//実測値
			boolean ret = RecordCheck.check(abnormal3);

			//検証
			assertThat(ret, is(expected));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void recordCheck異常系_BufferedReader_capacityNumber() {

		try {
			//期待値を設定(メソッドのreturnで返してほしい値)
			boolean expected = false;

			//実測値
			boolean ret = RecordCheck.check(abnormal4);

			//検証
			assertThat(ret, is(expected));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void recordCheck異常系_BufferedReader_micNumber() {

		try {
			//期待値を設定(メソッドのreturnで返してほしい値)
			boolean expected = false;

			//実測値
			boolean ret = RecordCheck.check(abnormal5);

			//検証
			assertThat(ret, is(expected));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void recordCheck異常系_BufferedReader_boardNumber() {

		try {
			//期待値を設定(メソッドのreturnで返してほしい値)
			boolean expected = false;

			//実測値
			boolean ret = RecordCheck.check(abnormal6);

			//検証
			assertThat(ret, is(expected));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void recordCheck異常系_BufferedReader_projectorNumber() {

		try {
			//期待値を設定(メソッドのreturnで返してほしい値)
			boolean expected = false;

			//実測値
			boolean ret = RecordCheck.check(abnormal7);

			//検証
			assertThat(ret, is(expected));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void recordCheck異常系_BufferedReader_closeNull() {

		try {
			//期待値を設定(メソッドのreturnで返してほしい値)
			boolean expected = false;

			//実測値
			boolean ret = RecordCheck.check(abnormal8);

			//検証
			assertThat(ret, is(expected));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void recordCheck異常系_BufferedReader_closeFormat() {

		try {
			//期待値を設定(メソッドのreturnで返してほしい値)
			boolean expected = false;

			//実測値
			boolean ret = RecordCheck.check(abnormal9);

			//検証
			assertThat(ret, is(expected));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}