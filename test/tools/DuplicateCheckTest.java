package tools;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

public class DuplicateCheckTest {

	FileInputStream fi = null;
	InputStreamReader is = null;
	BufferedReader br = null;
	String ok = "c:\\work\\room_20180601.csv";
	String FALSE = "c:\\work\\err101.csv";
	String CATCH = "c:\\work\\catch.csv";

	@Test
	public void DuplicateCheck正常系かぶりなし() throws IOException {
		boolean expect = true;
		boolean ret = DuplicateCheck.check(ok, 1);
		assertThat(ret, CoreMatchers.is(expect));
	}

	@Test
	public void DuplicateCheck異常系かぶりあり() throws IOException {
		boolean expect = false;
		boolean ret = DuplicateCheck.check(FALSE, 1);
		assertThat(ret, CoreMatchers.is(expect));

	}

	@Test
	public void DuplicateCheck異常系ありえない配列index() throws IOException {
		boolean expect = false;
		boolean ret = DuplicateCheck.check(ok, 100);
		assertThat(ret, CoreMatchers.is(expect));

	}

}
