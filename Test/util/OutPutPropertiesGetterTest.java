package util;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class OutPutPropertiesGetterTest {

	public static final String PATH = "C:\\Users\\user\\git\\eventmanager3\\src\\file_out_put.properties";
	public static final String EXPECT = "C:/Users/user/Desktop";

	@Test
	public void getProperties_正常系() throws IOException {

		OutPutPropertiesGetter outPutPropertiesGetter = new OutPutPropertiesGetter();
		String ret = outPutPropertiesGetter.getProperties(PATH);
		assertThat(ret, is(EXPECT));
	}

	@Test(expected = IOException.class)
	public void getProperties_異常系() throws IOException {

		OutPutPropertiesGetter outPutPropertiesGetter = new OutPutPropertiesGetter();
		outPutPropertiesGetter.getProperties(EXPECT);
	}

}
