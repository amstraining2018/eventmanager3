package tools;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class UserRecordCheckTest {

	//正常系で参照されるファイル
	String normal = "C:\\work\\account_20180601.csv";

	//異常系で参照されるファイル
	//異常系  employee_idが存在しない
	String abnormal1 = "C:\\work\\account_20180601_abnormal_employee_idNull.csv";
	//異常系  employee_idが10文字以上
	String abnormal2 = "C:\\work\\account_20180601_abnormal_employee_idOver10.csv";
	//異常系 login_idが存在しない
	String abnormal3 = "C:\\work\\account_20180601_abnormal_loginIdNull.csv";
	//異常系 login_idが10文字以上
	String abnormal4 = "C:\\work\\account_20180601_abnormal_loginIdOver10.csv";
	//異常系 passが存在しない
	String abnormal5 = "C:\\work\\account_20180601_abnormal_passNull.csv";
	//異常系 passが13文字以上
	String abnormal6 = "C:\\work\\account_20180601_abnormal_passOver12.csv";
	//異常系 passが8文字未満
	String abnormal7 = "C:\\work\\account_20180601_abnormal_passUnder8.csv";
	//異常系 typeIdが存在しない
	String abnormal8 = "C:\\work\\account_20180601_abnormal_typeIdNull.csv";
	//異常系 passが1文字以上
	String abnormal9 = "C:\\work\\account_20180601_abnormal_typeIdOver1.csv";
	//異常系 フォーマットが違うやつ
	String abnormal10 = "C:\\work\\account_20180601_abnormal_bounds.csv";

	@Test
	public void check_正常系() throws IOException {
		//期待値を設定(メソッドのreturnで返してほしい値)
		boolean expected = true;
		//実測値
		boolean ret = UserRecordCheck.check(normal);
		//検証
		assertThat(ret, is(expected));
	}

	@Test
	public void check_異常系_employee_idが存在しない() throws IOException {
		//期待値を設定(メソッドのreturnで返してほしい値)
		boolean expected = false;
		//実測値
		boolean ret = UserRecordCheck.check(abnormal1);
		//検証
		assertThat(ret, is(expected));
	}

	@Test
	public void check_異常系_employee_idが10文字以上() throws IOException {
		//期待値を設定(メソッドのreturnで返してほしい値)
		boolean expected = false;
		//実測値
		boolean ret = UserRecordCheck.check(abnormal2);
		//検証
		assertThat(ret, is(expected));
	}

	@Test
	public void check_異常系_login_idが存在しない() throws IOException {
		//期待値を設定(メソッドのreturnで返してほしい値)
		boolean expected = false;
		//実測値
		boolean ret = UserRecordCheck.check(abnormal3);
		//検証
		assertThat(ret, is(expected));
	}

	@Test
	public void check_異常系_login_idが10文字以上() throws IOException {
		//期待値を設定(メソッドのreturnで返してほしい値)
		boolean expected = false;
		//実測値
		boolean ret = UserRecordCheck.check(abnormal4);
		//検証
		assertThat(ret, is(expected));
	}

	@Test
	public void check_異常系_passが存在しない() throws IOException {
		//期待値を設定(メソッドのreturnで返してほしい値)
		boolean expected = false;
		//実測値
		boolean ret = UserRecordCheck.check(abnormal5);
		//検証
		assertThat(ret, is(expected));
	}

	@Test
	public void check_異常系_passが13文字以上() throws IOException {
		//期待値を設定(メソッドのreturnで返してほしい値)
		boolean expected = false;
		//実測値
		boolean ret = UserRecordCheck.check(abnormal6);
		//検証
		assertThat(ret, is(expected));
	}

	@Test
	public void check_異常系_passが8文字未満() throws IOException {
		//期待値を設定(メソッドのreturnで返してほしい値)
		boolean expected = false;
		//実測値
		boolean ret = UserRecordCheck.check(abnormal7);
		//検証
		assertThat(ret, is(expected));
	}

	@Test
	public void check_異常系_typeIdが存在しない() throws IOException {
		//期待値を設定(メソッドのreturnで返してほしい値)
		boolean expected = false;
		//実測値
		boolean ret = UserRecordCheck.check(abnormal8);
		//検証
		assertThat(ret, is(expected));
	}

	@Test
	public void check_異常系_typeIdが1文字以上() throws IOException {
		//期待値を設定(メソッドのreturnで返してほしい値)
		boolean expected = false;
		//実測値
		boolean ret = UserRecordCheck.check(abnormal9);
		//検証
		assertThat(ret, is(expected));
	}

	@Test
	public void check_異常系_OutOfBoundsException() throws IOException {
		//期待値を設定(メソッドのreturnで返してほしい値)
		boolean expected = false;
		//実測値
		boolean ret = UserRecordCheck.check(abnormal10);
		//検証
		assertThat(ret, is(expected));
	}
}
