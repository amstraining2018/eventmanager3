package tools;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class FileCheckImplTest {

	public static final String HEAD = "H";
	private static final String HEAD_ARRAY1 = "H,会議室名,収容人数,マイク,ホワイトボード,プロジェクター,施錠時間";
	private static final String HEAD_ARRAY2 = "H,社員ID,アカウント,初期パスワード,権限";
	private static final String HEAD_ARRAY3 = "H,あ,収容人数,マイク,ホワイトボード,プロジェクター,施錠時間";
	private static final String HEAD_ARRAY4 = "H,あ,アカウント,初期パスワード,権限";
	private static final String DATA_ARRAY = "D,第一会議室,10,1,1,1,17:00";
	private static final String END_ARRAY1 = "E,4,,,,,";
	private static final String END_ARRAY2 = "B,4,,,,,";
	private static final String END_ARRAY3 = "E";

	@Test
	public void headItem_正常系_H行をカンマ区切りにして数えたらもともとのフォーマットと同じ場合() {

		FileCheckImpl fileCheckImpl = new FileCheckImpl();
		Boolean expect = true;

		Boolean ret;
		ret = fileCheckImpl.headItem(HEAD_ARRAY1);
		assertThat(ret, is(expect));

	}

	@Test
	public void headItem_異常系1_H行をカンマ区切りにして数えたら元々のフォーマットと違う場合() {

		FileCheckImpl fileCheckImpl = new FileCheckImpl();
		Boolean expect = false;

		Boolean ret;
		ret = fileCheckImpl.headItem(HEAD_ARRAY2);
		assertThat(ret, is(expect));
	}

	@Test
	public void headItem_異常系2_H行の配列の数は一緒だけど中身が異なる場合() {

		FileCheckImpl fileCheckImpl = new FileCheckImpl();
		Boolean expect = false;

		Boolean ret;
		ret = fileCheckImpl.headItem(HEAD_ARRAY3);
		assertThat(ret, is(expect));

	}

	@Test
	public void dataItem_正常系_D行の項目数が正しい場合() {

		FileCheckImpl fileCheckImpl = new FileCheckImpl();
		Boolean expect = true;
		Boolean ret = fileCheckImpl.dataItem(HEAD_ARRAY1, DATA_ARRAY);
		assertThat(ret, is(expect));
	}

	@Test
	public void dataItem_異常系_D行の項目数が正しくない場合() {

		FileCheckImpl fileCheckImpl = new FileCheckImpl();
		Boolean expect = false;
		Boolean ret = fileCheckImpl.dataItem(HEAD_ARRAY2, DATA_ARRAY);
		assertThat(ret, is(expect));
	}

	@Test
	public void dataRowNum_正常系_D行件数とE行の数が正しい場合() {

		FileCheckImpl fileCheckImpl = new FileCheckImpl();
		Boolean expect = true;
		Boolean ret = fileCheckImpl.dataRowNum(4, END_ARRAY1);
		assertThat(ret, is(expect));
	}

	@Test
	public void dataRowNum_異常系1_D行数とE行の数が異なる場合() {

		FileCheckImpl fileCheckImpl = new FileCheckImpl();
		Boolean expect = false;
		Boolean ret = fileCheckImpl.dataRowNum(5, END_ARRAY1);
		assertThat(ret, is(expect));
	}

	@Test
	public void dataRowNum_異常系2_END行の最初がEじゃない場合() {

		FileCheckImpl fileCheckImpl = new FileCheckImpl();
		Boolean expect = false;
		Boolean ret = fileCheckImpl.dataRowNum(4, END_ARRAY2);
		assertThat(ret, is(expect));
	}

	@Test
	public void userHeadItem_正常系_H行をカンマ区切りにして数えたらもともとのフォーマットと同じ場合() {

		FileCheckImpl fileCheckImpl = new FileCheckImpl();
		Boolean expect = true;
		Boolean ret;
		ret = fileCheckImpl.userHeadItem(HEAD_ARRAY2);
		assertThat(ret, is(expect));
	}

	@Test
	public void userHeadItem_異常系1_H行をカンマ区切りにして数えたら元々のフォーマットと違う場合() {

		FileCheckImpl fileCheckImpl = new FileCheckImpl();
		Boolean expect = false;
		Boolean ret;
		ret = fileCheckImpl.userHeadItem(HEAD_ARRAY1);
		assertThat(ret, is(expect));
	}

	@Test
	public void userHeadItem_異常系2_H行の配列の数は一緒だけど中身が異なる場合() {

		FileCheckImpl fileCheckImpl = new FileCheckImpl();
		Boolean expect = false;
		Boolean ret;
		ret = fileCheckImpl.userHeadItem(HEAD_ARRAY4);
		assertThat(ret, is(expect));
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void userHeadItem_異常系_ArrayIndexOutOfBoundsException() {
		FileCheckImpl fileCheckImpl = new FileCheckImpl();
		fileCheckImpl.dataRowNum(5, END_ARRAY3);
	}

}
