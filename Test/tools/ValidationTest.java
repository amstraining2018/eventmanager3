package tools;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class ValidationTest {

	private static final String EMPTY = "";
	private static final String NULL = null;
	private static final String STR1 = "abcdefghijklmnop";
	private static final String STR2 = "a";
	private static final String STR3 = "あいうえおかきくけ";
	private static final String STR4 = "🍣🍖";
	private static final String STR5 = "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをん1234567890"
			+ "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをん1234567890";
	private static final String STDE1 = "S";
	private static final String STDE2 = "STDE";
	private static final String NUM1 = "123456789";
	private static final String NUM2 = "1";
	private static final String STNUM1 = "abc123";
	private static final String STNUM2 = "abc123def456";
	private static final String DATE1 = "12:00";
	private static final String DATE2 = "95:00";

	@Test
	public void checkNull_正常系_中身が存在する場合() {

		boolean expect = true;
		boolean ret = Validation.checkNull(STR1);
		assertThat(ret, is(expect));
	}

	@Test
	public void checkNull_異常系_中身が空文字の場合() {

		boolean expect = false;
		boolean ret = Validation.checkNull(EMPTY);
		assertThat(ret, is(expect));
	}

	@Test
	public void checkNull_異常系_中身がnullの場合() {

		boolean expect = false;
		boolean ret = Validation.checkNull(NULL);
		assertThat(ret, is(expect));
	}

	@Test
	public void chNum_正常系1_中身が数値である場合() {

		boolean expect = true;
		boolean ret = Validation.chNum(NUM1);
		assertThat(ret, is(expect));
	}

	@Test
	public void chNum_正常系2_中身が空文字である場合() {

		boolean expect = true;
		boolean ret = Validation.chNum(EMPTY);
		assertThat(ret, is(expect));
	}

	@Test
	public void chNum_異常系_中身が数値もしくは空文字でない場合() {

		boolean expect = false;
		boolean ret = Validation.chNum(STR1);
		assertThat(ret, is(expect));
	}

	@Test
	public void chNumDig_正常系_中身が1桁以上の半角数字の場合() {

		boolean expect = true;
		boolean ret = Validation.chNumDig(NUM1);
		assertThat(ret, is(expect));
	}

	@Test
	public void chNumDig_異常系1_中身が0桁の場合() {

		boolean expect = false;
		boolean ret = Validation.chNumDig(EMPTY);
		assertThat(ret, is(expect));
	}

	@Test
	public void chNumDig_異常系2_中身が1桁以上の半角数字でない場合() {

		boolean expect = false;
		boolean ret = Validation.chNumDig(STR1);
		assertThat(ret, is(expect));
	}

	@Test
	public void chNumDig1_正常系_中身が1桁の半角数字の場合() {

		boolean expect = true;
		boolean ret = Validation.chNumDig1(NUM2);
		assertThat(ret, is(expect));
	}

	@Test
	public void chNumDig1_異常系1_中身が2桁以上の半角数字の場合() {

		boolean expect = false;
		boolean ret = Validation.chNumDig1(NUM1);
		assertThat(ret, is(expect));
	}

	@Test
	public void chNumDig1_異常系2_中身が1桁の半角数字以外の場合() {

		boolean expect = false;
		boolean ret = Validation.chNumDig1(STR2);
		assertThat(ret, is(expect));
	}

	@Test
	public void chSHDEDig1_正常系_中身が1桁のSHDEのどれかである場合() {

		boolean expect = true;
		boolean ret = Validation.chSHDEDig1(STDE1);
		assertThat(ret, is(expect));
	}

	@Test
	public void chSHDEDig1_異常系1_中身が1桁以上のSHDEのどれかである場合() {

		boolean expect = false;
		boolean ret = Validation.chSHDEDig1(STDE2);
		assertThat(ret, is(expect));
	}

	@Test
	public void chSHDEDig1_異常系2_中身が1桁のSHDE以外の文字である場合() {

		boolean expect = false;
		boolean ret = Validation.chSHDEDig1(STR2);
		assertThat(ret, is(expect));
	}

	@Test
	public void chStrDig10_正常系_中身が10桁以内の半角英数字である場合() {

		boolean expect = true;
		boolean ret = Validation.chStrDig10(STNUM1);
		assertThat(ret, is(expect));
	}

	@Test
	public void chStrDig10_異常系1_中身が10桁以上の半角英数字である場合() {

		boolean expect = false;
		boolean ret = Validation.chStrDig10(STNUM2);
		assertThat(ret, is(expect));
	}

	@Test
	public void chStrDig10_異常系2_中身が10桁以内の半角英数字以外である場合() {

		boolean expect = false;
		boolean ret = Validation.chStrDig10(STR3);
		assertThat(ret, is(expect));
	}

	@Test
	public void chStrDig100ja_正常系_中身が100桁以内の文字列である場合() {

		boolean expect = true;
		boolean ret = Validation.chStrDig100ja(STR3);
		assertThat(ret, is(expect));
	}

	@Test
	public void chStrDig100ja_異常系1_中身が100桁以内の文字列に含まれない文字である場合() {

		boolean expect = false;
		boolean ret = Validation.chStrDig100ja(STR4);
		assertThat(ret, is(expect));
	}

	@Test
	public void chStrDig100ja_異常系2_中身が100桁以上の文字列である場合() {

		boolean expect = false;
		boolean ret = Validation.chStrDig100ja(STR5);
		assertThat(ret, is(expect));
	}

	@Test
	public void chStrDig812_正常系_中身が8から12桁以内の半角英数字である場合() {

		boolean expect = true;
		boolean ret = Validation.chStrDig812(STNUM2);
		assertThat(ret, is(expect));
	}

	@Test
	public void chStrDig812_異常系1_中身が8桁未満の半角英数字である場合() {

		boolean expect = false;
		boolean ret = Validation.chStrDig812(STNUM1);
		assertThat(ret, is(expect));
	}

	@Test
	public void chStrDig812_異常系2_中身が8から12桁以内の半角英数字以外である場合() {

		boolean expect = false;
		boolean ret = Validation.chStrDig812(STR3);
		assertThat(ret, is(expect));
	}

	@Test
	public void chStrDig812_異常系3_中身が13桁以上の半角英数字である場合() {

		boolean expect = false;
		boolean ret = Validation.chStrDig812(STR1);
		assertThat(ret, is(expect));
	}

	@Test
	public void chClose_正常系_時刻の形式が正しい場合() {

		boolean expect = true;
		boolean ret = Validation.chClose(DATE1);
		assertThat(ret, is(expect));
	}

	@Test
	public void chClose_異常系_時刻の形式が正しい場合() {

		boolean expect = false;
		boolean ret = Validation.chClose(DATE2);
		assertThat(ret, is(expect));
	}

}
