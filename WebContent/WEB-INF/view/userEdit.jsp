﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ管理</title>
<!--q ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="/eventManager3/view2/css/style.css" rel="stylesheet">
<link href="/eventManager3/view2/css/sticky-footer.css" rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet" />

<%
	String loginName = (String) session.getAttribute("loginName");
%>
</head>
<body>
	<div class="wrapper container">
		<!--q▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param name="kanri"
				value="3" /></jsp:include>

		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h2>ユーザ編集</h2>
			<!-- ▼ 入力フォーム -->
			<form action="UserEditServlet" method="post">
				<!-- ▼ 情報表示 -->
				<p>
					<input type="hidden" name="userId" class="form-control"
						value="${user.userId }" />
				</p>

				<label>パスワード (変更の場合のみ)</label>
				<p>
					<input  type="password" pattern="^([a-zA-Z0-9]{8,})$"
					title="8～12文字の半角英数字で入力してください" maxlength="12" name="loginPass" class="form-control"
						value="" />
				</p>
				<c:if test="${user.typeId!=1 }">
					<!-- 変更ユーザが管理者でない場合、ユーザ権限変更を出力-->
					<div class="form-group row form-check">
						<p>ユーザー権限</p>
						<input type="radio" name="typeId" value="2" checked="checked" />
						一般ユーザー <input type="radio" name="typeId" value="1" /> 管理者
					</div>
				</c:if>
				<c:if test="${user.typeId=='1'}">
					<!-- 変更ユーザが管理者の場合、typeId=1を送信-->
					<input type="hidden" name="typeId" value="1" />
				</c:if>
				<!-- ▼ ユーザ編集のキャンセルボタン -->
				<a href="UserDetailServlet?key=${user.userId }"
					class="btn btn-default">キャンセル</a>
				<button type="submit" name="" value="" class="btn btn-primary">保存</button>
				<!-- beforeGroupNameに値を持たせて送信 -->

			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
	<!-- ▼ jQuery … CDNから取得 -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
	<script>
		if (!window.jQuery) {
			document
					.write('<script src="/eventManager3/view2/js/jquery-3.3.1.min.js"><\/script>');
		}
	</script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="/eventManager3/view2/js/dropdown.js"></script>
	<script src="/eventManager3/view2/js/jquery-3.3.1.min.js"></script>

</body>
</html>