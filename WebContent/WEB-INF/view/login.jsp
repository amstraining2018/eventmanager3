<%@ page language="java"
contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%--
ページディレクティブ
属性：language、contentType、pageEncoding
--%>

<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>


<link href="/eventManager3/view2/css/style.css" rel="stylesheet">
<link href="/eventManager3/view2/css/sticky-footer.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
<title>ログイン</title>

</head>

<body class="login-body">
    <div class="wrapper login-wrapper container">


<main class="login-contents col-sm-8 col-sm-offset-2 col-xs-12">

<article>
<div class="panel login-panel panel-default">

<div class="panel-heading">
 Event Manager
 </div>

<div class="panel-body">
<form action="LoginServlet" method="post">

<p>
<%
boolean err = (boolean)request.getAttribute("error");
if(err == true){
%>
<%--★★ID、パスワードが誤っていた際の注意文★★--%>
<p class=warn>ログインIDかパスワードが誤っています</p>
<%} %>
<p>
<input type="text"  pattern="^[0-9A-Za-z|_|-]+$"  title="半角英数字、ハイフン、アンダーバーのみ入力可能です"
 required maxlength="10" name="loginId" class="form-control" placeholder="ログインID"/>
</p>
<p>
<input type="password" pattern="^([0-9A-Za-z]{8,})$" title="パスワードは8～12文字の半角英数字のみ入力可能です"
  required maxlength="12" name="loginPass" class="form-control" placeholder="パスワード"/>
</p>

<%--
ユーザーの入力受付input要素のtextタイプ　Part2（p39,40）
--%>

<button type="submit" class="btn btn-block btn-primary">ログイン</button>

<%--
ログインボタンPart2（p146,147）
--%>

</form>
</div>

</div>
</article>

</main>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
 <script>
		if (!window.jQuery) {
			document.write('<script src="/Team03/view/js/jquery-3.3.1.min.js"><\/script>');
		}
</script>

<script>
<%--★★ID、パスワードが誤っていた際の注意文の色★★--%>
$(document).ready(function(){
$(".warn").css("color","red");
});
</script>
<script src="/eventManager3/view2/js/jquery-3.3.1.min.js"></script>
</body>

</html>