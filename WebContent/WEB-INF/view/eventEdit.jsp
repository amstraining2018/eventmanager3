<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:useBean id="start" class="java.util.Date" />

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>イベント管理</title>
<!-- a▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- q▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="/eventManager3/view2/css/style.css" rel="stylesheet">
<link href="/eventManager3/view2/css/sticky-footer.css" rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet" />
</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param name="kanri"
				value="2" /></jsp:include>
		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h2>イベント編集</h2>
			<!-- ▼ 入力フォーム -->
			<form action="eventEdit" method="post">
				<!-- formの開始 -->
				<!-- ▼ 情報表示 -->
				<p>
					<input type="hidden" name="eventId" value="${event.eventId }">
					<!-- eventIdの値を隠して保持-->
				</p>
				<label>タイトル (必須)</label>
				<p>
					<input type="text" required maxlength="50" name="title" class="form-control"
						value="${event.title}" />
					<!--title表示、title入力-->
				</p>
				<label>開始日時 (必須)</label>
				<p>
					<fmt:formatDate value="${event.start}" var="startFormat"
						pattern="yyyy-MM-dd HH:mm" />
					<!--patternのフォーマットでstartの値を取得 -->
					<input type="text" pattern="([1-2]{1}[0-9]{3})-([0-1][0-9])-([0-3][0-9]) ([0-2][0-9])(:[0-5][0-9])"
					 required title=" 0000-00-00 00:00 (半角数字、半角スペース、ダブルコロン、ハイフンのみ入力可能です)"
					 maxlength="16" name="start" class="form-control" placeholder="2018-01-01 00:00" value="${startFormat}" />
					<!--start表示、start時間入力-->
					<%
						int err = (Integer) request.getAttribute("err");//errの値を取得
						if (err == -1) {//-1の場合以下のコメントを出力
					%>
				<p class="text-danger">開始日時に過去の時間が設定されています</p>
				<%
					}
				%>
				</p>
				<label>終了日時 (必須)</label>
				<p>
					<fmt:formatDate value="${event.end}" var="endFormat"
						pattern="yyyy-MM-dd HH:mm" />
					<input type="text" pattern="([1-2]{1}[0-9]{3})-([0-1][0-9])-([0-3][0-9]) ([0-2][0-9])(:[0-5][0-9])"
					 required title=" 0000-00-00 00:00 (半角数字、半角スペース、ダブルコロン、ハイフンのみ入力可能です)"
					  maxlength="16" name="end" class="form-control" placeholder="2018-01-01 00:00"
					 value="${endFormat }" />
						</p>
					<!--end表示、end時間入力-->
					<%
						int err2 = (Integer) request.getAttribute("err2");//err2の値を取得
						if (err2 == -1) {//-1の場合以下のコメントを出力
					%>
				<p class="text-danger">終了日時に過去の時間が設定されています</p>
				<%
					} else if (err2 == -2) {//-2の場合以下のコメントを出力
				%>
				<p class="text-danger">開始時間より前の時間が設定されています</p>
				<%
					}
				%>
				<label>場所 (必須)</label>
				<div class="choose-group js-choose-group">
					<select class="form-control" name="roomId">
						<c:forEach items="${rooms }" var="room">
							<option value="<c:out value ="${room.id}"/>">
								<c:out value="${room.name }" />
							</option>
						</c:forEach>
					</select>
				</div>
				</p>
				<label>対象グループ</label>
				<div class="choose-group js-choose-group">

					<select class="form-control" name="eventsGroupId">
						<c:forEach items="${groups }" var="group">
							<option value="<c:out value ="${group.id}"/>">
								<c:out value="${group.name }" />
							</option>
						</c:forEach>
					</select>

				</div>
				<label>詳細</label>
				<p>
					<textarea maxlength="500" name="detail" class="form-control textarea-width"
						><c:out value="${event.detail}" /></textarea>
					<!--detail表示、detail入力-->
				</p>

				<a href="EventDetail?key=${event.eventId }" class="btn btn-default">キャンセル</a>
				<!--keyの値を持たせEventDetailに遷移-->
				<button type="submit" name="" value="" class="btn btn-primary">保存</button>
				<!--eventEditに値を送信-->
				<p>
					<input type="hidden" name="registeredById"
						value="${event.registeredById }">
					<!-- registeredByIdの値を隠して保持-->
				</p>
			</form>
		</article>
		</main>
	</div>


	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
	<!-- ▼ jQuery … CDNから取得 -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
	<script>
		if (!window.jQuery) {
			document
					.write('<script src="/eventManager3/view2/js/jquery-3.3.1.min.js"><\/script>');
		}
	</script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="/eventManager3/view2/js/jquery-3.3.1.min.js"></script>
	<script src="/eventManager3/view2/js/dropdown.js"></script>
	<script src="/eventManager3/view2/js/jquery-3.3.1.min.js"></script>

</body>
</html>