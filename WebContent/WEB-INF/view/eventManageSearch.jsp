<%@ page
	language="java"
	contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib
	prefix="c"
	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib
	prefix="fmt"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	request.setCharacterEncoding("UTF-8");
	String totalNumberOfPages = null;
	totalNumberOfPages = request.getAttribute("totalNumberOfPages").toString();
%>
<%
	String currentPageNumber = null;
		currentPageNumber = request.getAttribute("currentPageNumber").toString();
%>



<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta
	http-equiv="X-UA-Compatible"
	content="IE=edge" />
<meta
	name="viewport"
	content="width=device-width, initial-scale=1.0" />
<link
	href="<%= request.getContextPath() %>/view2/css/style.css"
	rel="stylesheet">
<link
	href="<%= request.getContextPath() %>/view2/css/sticky-footer.css"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet" />
<title>イベント管理</title>


</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param
				name="kanri"
				value="2" /></jsp:include>
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h1>イベント検索結果</h1>

			<div class="container">
				<form
					class="form-horizontal"
					action=""
					method="post">

					<input
						type="hidden"
						name="groupId"
						value="<c:out value="${groupID}"/>">

					<input
						type="hidden"
						name="roomId"
						value="<c:out value="${roomID}"/>">

					<input
						type="hidden"
						id="overRideCheckFlugTag"
						name="overRideCheckFlug"
						value="true">

					<div class="form-groups ">
						<label
							for="group"
							class="control-label col-sm-1 text-right"
							style="padding-right: 0px; padding-right: 0px; margin-top: 5px;">所属</label>
						<div class="col-sm-3 choose-group js-choose-group">
							<select
								class="form-control"
								id="group"
								name="eventsGroupId">
								<option value="0">指定なし</option>
								<c:forEach
									items="${groups }"
									var="group">

									<c:choose>
										<c:when test="${groupID==group.id }">
											<option
												value="<c:out value ="${group.id}"/>"
												selected>
												<c:out value="${group.name }" />
											</option>
										</c:when>
										<c:otherwise>
											<option value="<c:out value ="${group.id}"/>">
												<c:out value="${group.name }" />
											</option>
										</c:otherwise>
									</c:choose>

								</c:forEach>
							</select>
						</div>
					</div>
					<!-- <div class="form-group"> -->
					<label
						for="group"
						class="control-label col-sm-1 text-right"
						style="padding-right: 0px; padding-right: 0px; margin-top: 5px;">場所</label>
					<div class="col-sm-3 choose-group js-choose-group">
						<select
							class="form-control"
							name="eventsRoomId">
							<option value="0">指定なし</option>
							<c:forEach
								items="${rooms }"
								var="room">

								<c:choose>
									<c:when test="${roomID==room.id }">
										<option
											value="<c:out value ="${room.id}"/>"
											selected>
											<c:out value="${room.name }" />
										</option>
									</c:when>
									<c:otherwise>
										<option value="<c:out value ="${room.id}"/>">
											<c:out value="${room.name }" />
										</option>
									</c:otherwise>
								</c:choose>

							</c:forEach>
						</select>


					</div>
					<!-- </div> -->
					<div class="col-md-4">
						<button
							type="submit"
							onclick="submitAction('EventSearchServlet')"
							class="btn btn-primary">検索</button>
						<button
							onclick="submitAction('FileOutPutServlet')"
							class="btn btn-warning">CSV出力</button>
					</div>


					<div
						class="modal fade"
						id="confirmModal"
						tabindex="-1">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-body">
									<button
										type="button"
										class="close"
										data-dismiss="modal">
										<span>×</span>
									</button>
									<p>
										すでにファイルが存在します。
										<br>
										上書きしますか？
									</p>
								</div>
								<div class="modal-footer">
									<button
										class="btn btn-default"
										data-dismiss="modal">Cancel</button>
									<button
										id="modalOKButton"
										type="submit"
										class="btn btn-primary">OK</button>
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
			<nav class="clearfix">
				<ul class="pagination pull-left">
					<li class="page-item">
						<a
							aria-level="前ページへ"
							href="EventSearchServlet?
							currentPageNumber=
							<%if (Integer.parseInt(currentPageNumber) <= 1) {%>
							<%=1%>
							<%} else {%>
							<%=Integer.parseInt(currentPageNumber) - 1%><%}%>
							&eventsRoomId=<c:out value="${roomID}" />
							&eventsGroupId=<c:out value="${groupID}" />">
							<span aria-hidden="true">≪</span>
						</a>
					</li>

					<%
						for (int i = 1; i <= Integer.parseInt(totalNumberOfPages); i++) {
							if (i == Integer.parseInt(currentPageNumber)) {
					%>
					<li class="active">
						<%
							} else {
						%>

					<li>
						<%
							}
						%>
						<a
							href="EventSearchServlet?
							currentPageNumber=<%=i%>
							&eventsRoomId=<c:out value="${roomID}" />
							&eventsGroupId=<c:out value="${groupID}" />">
							<%=i%>
						</a>
					</li>

					<%
						}
					%><li>
						<a
							aria-label="次のページへ"
							href="EventSearchServlet
							?currentPageNumber=
							<%if (Integer.parseInt(currentPageNumber) >= (Integer.parseInt(totalNumberOfPages))) {%>
							<%=totalNumberOfPages%>
							<%} else {%>
							<%=Integer.parseInt(currentPageNumber) + 1%><%}%>
							&eventsRoomId=<c:out value="${roomID}" />
							&eventsGroupId=<c:out value="${groupID}" />">
							<span aria-hidden="true">≫</span>
						</a>
					</li>
				</ul>
			</nav>
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th class="col-md-3">タイトル</th>
							<th class="col-md-4">開始日時</th>
							<th class="col-md-2">場所</th>
							<th class="col-md-2">対象グループ</th>
							<th class="col-md-1">詳細</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach
							items="${EventList}"
							var="Event">
							<tr>
								<td class="col-md-3"><c:out value="${Event.title}" /> <c:if test="${ Event.attendtf}">
										<span class="label label-danger">参加</span>
									</c:if></td>
								<td class="col-md-4"><fmt:parseDate
										value="${Event.start }"
										pattern="yyyy-MM-dd HH:mm:ss"
										var="start" /> <fmt:formatDate
										value="${start }"
										var="startFormat"
										pattern="yyyy年MM月dd日(E) HH時mm分" /> <c:out value="${startFormat}" /></td>
								<td class="col-md-2"><c:out value="${Event.place}" /></td>
								<td class="col-md-2"><c:out value="${Event.groupName}" /></td>
								<td class="col-md-1">
									<form
										action="EventDetail"
										method="get">
										<button
											type="submit"
											class="btn btn-default"
											value="${Event.eventId }"
											name="key">詳細</button>
									</form>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<!-- 一回a hrefにしたので不具合があったら戻して下さい
					<form action="EventRegistServlet" method="get">
						<input type="submit" class="btn btn-primary" value="イベントの登録" />
					</form>
					-->
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
	<!-- ▼ jQuery … CDNから取得 -->
	<script
		src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
	<script>if (!window.jQuery){ document.write('<script src="<%=request.getContextPath()%>
		/view2/js/jquery-3.3.1.min.js"><\/script>');
		}
	</script>
	<!-- 下の方法でbootstrap.min.jsを読み込むとmodalがうまく動作しない -->
	<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script> -->

	<script src="<%=request.getContextPath()%>/view2/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/view2/js/jquery-3.3.1.min.js"></script>
	<script src="<%=request.getContextPath()%>/view2/js/dropdown.js"></script>

	<script>
	function submitAction(url) {
		$('form').attr('action', url);
		$('form').submit();
	};
	</script>



	<script>
		$(document).ready(function() {
			$('#modalOKButton').click(function() {
				$('form').attr('action', 'FileOutPutServlet');
				$('#overRideCheckFlugTag').attr('value', 'false');
				$('form').submit();
			});
			<c:if test="${overRideError==true }">
			$('#test_button').click();
			</c:if>
			<c:if test="${notExistPropertieError==true }">
			alert("プロパティファイルが読み込めませんでした");
			</c:if>
		});
	</script>


	<input
		type="hidden"
		id="test_button"
		data-toggle="modal"
		data-target="#confirmModal">


</body>
</html>