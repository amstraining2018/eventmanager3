﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/view2/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/view2/css/sticky-footer.css"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet" />
<title>ユーザ管理</title>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param name="kanri"
				value="3" /></jsp:include>
		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h2>ユーザ登録</h2>
			<!-- ▼ 入力フォーム -->
			<form action="UserRegistServlet" method="post">
				<!-- ▼ 情報表示 -->
				<label>社員ID</label>

				<div class="form-inline">
					<select class="form-control" id="" name="employeeId">
						<c:forEach items="${employeeList }" var="employee">
							<option value="<c:out value ="${employee.employee_id}"/>">
								<c:out value="${employee.employee_id}" />
							</option>
						</c:forEach>
					</select>
				</div>
				<%
					int err;
					err = (Integer) request.getAttribute("err");
					if (err == -2) {
				%>

				<p class="text-danger">社員IDの入力がありません</p>
				<%
					}
				%>
				<label>ログインID (必須)</label>

				<%
					err = (Integer) request.getAttribute("err");
					if (err == -1) {
				%>

				<p class="text-danger">すで登録済みのログインIDのため登録ができません</p>
				<%
					}
				%>
				<p>
					<input type="text" pattern="^[0-9A-Za-z|_|-]+$" title="半角英数字、ハイフン、アンダーバーのみ入力可能です"
					 required maxlength="10"  name="login_id" class="form-control"
						placeholder="ログインID" />
				</p>
				<label>パスワード (必須)</label>
				<p>
					<input type="password" pattern="^([0-9A-Za-z]{8,})$"
					title="8～12文字の半角英数字で入力してください"
					required maxlength="12" name="login_pass" class="form-control"
						placeholder="パスワード" />
				</p>
				<label>ユーザー権限</label>
				<div class="form-group row form-check">
					<input type="radio" name="typeId" value="2" checked="checked" />
					一般ユーザー <input type="radio" name="typeId" value="1" /> 管理者
				</div>

				<!-- ▼ ユーザの登録キャンセルボタン -->
				<!--
            <button type="submit" name="" value="" class="btn btn-default">キャンセル</button>
            -->
				<a href="UserManageServlet" class="btn btn-default">キャンセル</a>
				<!-- ▼ ユーザの登録ボタン -->
				<!--
            <button type="submit" name="" value="" class="btn btn-primary">登録</button>
            -->
				<input type="submit" value="登録" class="btn btn-primary">
			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
	<!-- ▼ jQuery … CDNから取得 -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
	<script>
		if (!window.jQuery) {
			document
					.write('<script src="${pageContext.request.contextPath}/view2/js/jquery-3.3.1.min.js"><\/script>');
		}
	</script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/view2/js/jquery-3.3.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/view2/js/dropdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/view2/js/jquery-3.3.1.min.js"></script>

</body>
</html>
