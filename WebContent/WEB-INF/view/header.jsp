<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String kanri = request.getParameter("kanri");
%>
<link href="${pageContext.request.contextPath}/view2/css/style.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/view2/css/sticky-footer.css" rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet" />
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/view2/js/jquery-3.3.1.min.js"></script>
<script src="${pageContext.request.contextPath}/view2/js/dropdown.js"></script>
<body>
	<!-- ▼ ヘッダ -->
	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="navbar-header">
				<!-- ▼ ハンバーガー ボタン -->
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#pageNavbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a href="#" class="navbar-brand">Event Manager</a>
			</div>
			<!-- ▼ グローバルナビ -->
			<div class="collapse navbar-collapse" id="pageNavbar">
				<ul class="nav navbar-nav">
					<%
						if (kanri.equals("1")) {
					%>
					<li class="active"><a href="TodaysEventServlet">本日のイベント</a></li>
					<%
						} else {
					%>


					<li class=""><a href="TodaysEventServlet">本日のイベント</a></li>
					<%
						}
					%>

					<%
						if (kanri.equals("2")) {
					%>
					<li class="active"><a href="EventManageServlet">イベント管理</a></li>
					<%
						} else {
					%>


					<li class=""><a href="EventManageServlet">イベント管理</a></li>
					<%
						}
					%>

					<c:if test="${loginUserType==1}">
						<%
							if (kanri.equals("3")) {
						%>
						<li class="active"><a href="UserManageServlet">ユーザ管理</a></li>
						<%
							} else {
						%>


						<li class=""><a href="UserManageServlet">ユーザ管理</a></li>
						<%
							}
						%>
					</c:if>
				</ul>
				<ul class="nav navbar-nav navbar-right margin-navbar-right">
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown"> <span class="glyphicon glyphicon-user"></span>
							<c:out value="${loginUserName}" /> <span class="caret"></span>
					</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="PasswordChangeServlet">パスワード変更</a></li>
							<li><a href="LogoutServlet">ログアウト</a></li>
						</ul></li>
				</ul>
			</div>
		</nav>
	</header>
</body>