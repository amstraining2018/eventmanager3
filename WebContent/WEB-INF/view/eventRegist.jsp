﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="/eventManager3/view2/css/style.css" rel="stylesheet">
<link href="/eventManager3/view2/css/sticky-footer.css" rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet" />
<title>イベント管理</title>


</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param name="kanri"
				value="2" /></jsp:include>
		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h2>イベント登録</h2>
			<!-- ▼ 入力フォーム -->
			<form action="EventRegistServlet" method="post">
				<!-- ▼ 情報表示 -->
				<label>タイトル (必須)</label>
				<p>
					<input type="text" required maxlength="50" name="title" class="form-control" placeholder="" />
				</p>

				<label>開始日時 (必須)</label>
				<p>
					<input type="text" pattern="([1-2]{1}[0-9]{3})-([0-1][0-9])-([0-3][0-9]) ([0-2][0-9])(:[0-5][0-9])"
					 required title=" 0000-00-00 00:00 (半角数字、半角スペース、ダブルコロン、ハイフンのみ入力可能です)"
					 maxlength="16" name="start" class="form-control" placeholder="2018-01-01 00:00" />
				</p>
				<%
					int err = (Integer) request.getAttribute("err");
					if (err == -1) {
				%>
				<p class="text-danger">開始日時に過去の時間が設定されています</p>
				<%
					}
				%>

				<label>終了日時 (必須)</label>
				<p>
					<input type="text" pattern="([1-2]{1}[0-9]{3})-([0-1][0-9])-([0-3][0-9]) ([0-2][0-9])(:[0-5][0-9])"
					 title=" 0000-00-00 00:00 (半角数字、半角スペース、ダブルコロン、ハイフンのみ入力可能です)"
					 maxlength="16" name="end" class="form-control" placeholder="2018-01-01 00:00" />
				</p>
				<%
					int err2 = (Integer) request.getAttribute("err2");
					if (err2 == -1) {
				%>
				<p class="text-danger">終了日時に過去の時間が設定されています</p>
				<%
					} else if (err2 == -2) {
				%>
				<p class="text-danger">開始時間より前の時間が設定されています</p>
				<%
					}
				%>
				<label>場所 (必須)</label>
				<div class="choose-group js-choose-group">
					<select class="form-control" name="roomId">
						<c:forEach items="${rooms }" var="room">
							<option value="<c:out value ="${room.id}"/>">
								<c:out value="${room.name }" />
							</option>
						</c:forEach>
					</select>
				</div>
				<label>対象グループ</label>
				<div class="choose-group js-choose-group">

					<select class="form-control" name="eventsGroupId">
						<c:forEach items="${groups }" var="group">
							<option value="<c:out value ="${group.id}"/>">
								<c:out value="${group.name }" />
							</option>
						</c:forEach>
					</select>

				</div>
				<label>詳細</label>
				<p>
					<textarea maxlength="500" name="detail" class="form-control textarea-width"></textarea>
				</p>
				<!-- ▼ イベントの登録キャンセルボタン -->
				<!--
            <button type="submit" name="" value="" class="btn btn-default">キャンセル</button>
            -->
				<!-- イベント登録ボタンを押すと、EventRegistServletのdoPostメソッドに入る -->
				<a href="EventManageServlet" class="btn btn-default">キャンセル</a>
				<!-- ▼ イベントの登録ボタン -->
				<input type="submit" value="登録" class="btn btn-primary">
			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
	<!-- ▼ jQuery … CDNから取得 -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<!-- ▼ jQuery … jQueryの定義に失敗込めない?)した場合はローカルのファイルを読込む -->
	<script>
		if (!window.jQuery) {
			document
					.write('<script src="/eventManager3/view2/js/jquery-3.3.1.min.js"><\/script>');
		}
	</script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="/eventManager3/view2/js/jquery-3.3.1.min.js"></script>
	<script src="/eventManager3/view2/js/dropdown.js"></script>

</body>
</html>
