<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="${pageContext.request.contextPath}/view2/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/view2/css/sticky-footer.css"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet" />

<title>パスワード変更</title>

</head>



<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<jsp:include page="header.jsp"><jsp:param name="kanri"
				value="" /></jsp:include>
		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h1>マイページ</h1>
			<h3>パスワード変更</h3>
			<!-- ▼ 入力フォーム -->
			<form action="PasswordChangeServlet" method="post">
				<!-- ▼ 情報表示 -->

				<label>パスワード</label>
				<c:if test="${change!=null}">
					<div class="alert alert-success" role="alert">パスワードが更新されました</div>
				</c:if>
				<p>
					<input  type="password" required pattern="^([a-zA-Z0-9]{8,})$"
					title="8～12文字の半角英数字で入力してください"
					 maxlength="12" name="pass" class="form-control"
						placeholder="パスワード" />
				</p>

				<!-- ▼ ユーザ編集のキャンセルボタン -->
				<!--
            <button type="submit" name="" value="" class="btn btn-default">キャンセル</button>
            -->
				<a href="UserManageServlet" class="btn btn-default">キャンセル</a>
				<!-- ▼ ユーザ編集の保存ボタン -->
				<button type="submit" name="" value="" class="btn btn-primary">保存</button>
			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
	<!-- ▼ jQuery … CDNから取得 -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
	<script>
		if (!window.jQuery) {
			document
					.write('<script src="/eventManager3/view2/js/jquery-3.3.1.min.js"><\/script>');
		}
	</script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="/eventManager3/view2/js/jquery-3.3.1.min.js"></script>
	<script src="/eventManager3/view2/js/dropdown.js"></script>
</body>
</html>
