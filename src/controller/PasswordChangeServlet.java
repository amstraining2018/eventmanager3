package controller;

import static tools.Constants.*;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.UserDao;

/**
 * Servlet implementation class PasswordChangeServlet
 */
@WebServlet("/PasswordChangeServlet")
public class PasswordChangeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//		エラーメッセージ初期設定
		request.setAttribute("err", SAFE);
		request.setAttribute("err2", SAFE);

		//		パスワード変更画面に遷移
		request.getRequestDispatcher("WEB-INF/view/passwordChange.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//userIdの値を取得
		Integer id = (Integer) request.getSession().getAttribute("loginUserId");
		UserDao userDao = DaoFactory.createUserDao();
		try {
			//			jspで入力されたパスを取得してハッシュ化
			String psw = request.getParameter("pass");
			String pass = userDao.makePasswordHash(psw);
			//			ハッシュ化したパスワードとidの情報をパスワード変更メソッドに送る
			userDao.chanegePassword(pass, id);
			//			パスワード変更完了用メッセ
			request.setAttribute("change", "safe");
			request.getRequestDispatcher("WEB-INF/view/passwordChange.jsp").forward(request, response);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

}
