package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendDao;
import dao.DaoFactory;
import dao.EventDao;
import dao.UserDao;
import domain.Attend;

/**
 * Servlet implementation class UserDeleteServlet
 * test用コメント
 * test
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDeleteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//送られてきたuserIdを取得
		int id = Integer.parseInt(request.getParameter("userId"));
		try {
			AttendDao attendDao = DaoFactory.createAttendDao();
			UserDao userDao = DaoFactory.createUserDao();
			EventDao eventDao = DaoFactory.createEventDao();

			Attend attend = new Attend();

			attend.setAttendUserId(id);
			//usersテーブルから削除
			userDao.delete(id);
			//userが登録したevent情報をeventsテーブルから削除
			eventDao.deleteFindUserId(attend);
			//attendテーブルからuser_idで検索削除
			attendDao.attendDeleteUserId(attend);

			request.getRequestDispatcher("WEB-INF/view/userDeleteComp.jsp").forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

}
