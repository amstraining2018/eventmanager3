package controller;

import static tools.Constants.*;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.EmployeeDao;
import dao.UserDao;
import domain.Employee;
import domain.User;

/**
 * Servlet implementation class UserRegistServlet
 */
@WebServlet("/UserRegistServlet")
public class UserRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserRegistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {

			//EmployeeDaoインスタンスの作成
			EmployeeDao employeeDao = DaoFactory.createEmployeeDao();

			//開催場所の項目のための情報を取得、転送
			List<Employee> employeeList = employeeDao.findUnregistered();
			request.setAttribute("employeeList", employeeList);

			//		エラー用の初期設定をセットする
			int err = SAFE;
			request.setAttribute("err", err);
			request.getRequestDispatcher("WEB-INF/view/userRegist.jsp").forward(request, response);

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//エラー表示用の変数初期化
		int err = SAFE;
		request.setAttribute("err", err);
		String loginId = "loginId";
		boolean ret = false;
		UserDao GroupDao = DaoFactory.createUserDao();
		//社員IDを取得
		String employeeId = request.getParameter("employeeId");
		if (employeeId == null) {
			err = NO_EMPLOYEEID_ERROR;
			request.setAttribute("err", err);
			request.getRequestDispatcher("WEB-INF/view/userRegist.jsp").forward(request, response);
		} else {
			//ログインIDを取得
			String login_id = request.getParameter("login_id");
			//ユーザーのログインIDにかぶりがないかチェック
			try {
				ret = GroupDao.checkExsist(loginId, login_id);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			if (!ret) {
				//	もし入力されたログインIDがすでに存在していれば
				err = USER_ID_DUPLICATE;
				request.setAttribute("err", err);
				request.getRequestDispatcher("WEB-INF/view/userRegist.jsp").forward(request, response);
			} else {

				String login_pass;
				try {
					//			入力されたパスワードをハッシュ化
					login_pass = GroupDao.makePasswordHash(request.getParameter("login_pass"));
				} catch (Exception e1) {
					throw new ServletException(e1);
				}
				//ユーザ権限を取得
				Integer typeId = Integer.parseInt(request.getParameter("typeId"));

				User user = new User();
				user.setEmployee_id(employeeId);
				user.setLogin_id(login_id);
				user.setPass(login_pass);
				user.setTypeId(typeId);

				try {
					UserDao UserDao = DaoFactory.createUserDao();
					//			insertメソッドにリストを送る
					UserDao.insert(user);
					request.getRequestDispatcher("WEB-INF/view/userRegistComp.jsp").forward(request, response);
				} catch (Exception e) {
					throw new ServletException(e);
				}
			}
		}
	}
}
