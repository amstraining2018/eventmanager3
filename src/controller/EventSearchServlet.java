package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendDao;
import dao.DaoFactory;
import dao.EventSearchDao;
import dao.GroupListDao;
import dao.RoomDao;
import domain.Attend;
import domain.Event;
import domain.Group;
import domain.Room;
import tools.Page;

/**
 * Servlet implementation class EventSearchServlet
 */
@WebServlet("/EventSearchServlet")
public class EventSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventSearchServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			//Daoの作成
			AttendDao AttendDao = DaoFactory.createAttendDao();
			EventSearchDao EventSearchDao = DaoFactory.createEventSearchDao();

			//ログインしているユーザのログインIDを取得
			String loginId = (String) request.getSession().getAttribute("loginId");

			//絞り込みを行う所属グループのID
			int groupID = Integer.parseInt(request.getParameter("eventsGroupId"));
			request.setAttribute("groupID", groupID);

			//絞り込みを行う会議室のID
			int roomID = Integer.parseInt(request.getParameter("eventsRoomId"));
			request.setAttribute("roomID", roomID);

			//全ページ数を計算する。
			int numberOfTableRow = EventSearchDao.getNumberOfTableRow(groupID, roomID);
			int totalNumberOfPages = Page.allPageNumber(numberOfTableRow);
			request.setAttribute("totalNumberOfPages", totalNumberOfPages);

			//表示するページ番号を取得する
			String stringCurrentPageNumber = request.getParameter("currentPageNumber");
			int currentPageNumber = 0;
			if (stringCurrentPageNumber == null) {
				currentPageNumber = 1;
			} else {
				currentPageNumber = Integer.parseInt(stringCurrentPageNumber);
			}
			request.setAttribute("currentPageNumber", currentPageNumber);

			//loginしたユーザが参加するイベントをリストにする
			List<Attend> attendList = AttendDao.findByLoginId(loginId);

			//イベントの一覧と、ログインしているユーザが参加するイベントのリストをEventListにセット
			List<Event> EventList = EventSearchDao.search(groupID, roomID, currentPageNumber, attendList);

			//roomListDaoインスタンスの作成
			RoomDao roomDao = DaoFactory.createRoomDao();

			//開催場所の項目のための情報を取得、転送
			List<Room> rooms = roomDao.findAll();
			request.setAttribute("rooms", rooms);

			//groupListDaoインスタンスの作成
			GroupListDao groupListDao = DaoFactory.createGroupListDao();

			//開催場所の項目のための情報を取得、転送
			List<Group> groups = groupListDao.findAll();
			request.setAttribute("groups", groups);

			request.setAttribute("EventList", EventList);
			request.getRequestDispatcher("WEB-INF/view/eventManageSearch.jsp").forward(request,
					response);//jspにページ番号とページ数を送る

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);

	}

}
