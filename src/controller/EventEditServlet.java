package controller;

import static tools.Constants.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendDao;
import dao.DaoFactory;
import dao.EventDao;
import dao.GroupListDao;
import dao.RoomDao;
import domain.Attend;
import domain.Event;
import domain.Group;
import domain.Room;
import tools.TimeEr;

/**
 * Servlet implementation class EventEditServlet
 */
@WebServlet({ "/eventEdit", "/EventEditServlet" })
public class EventEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventEditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//インスタンスを作成
		Event event = new Event();

		//eventIdをgetParameterで取得
		Integer eventId = Integer.valueOf(request.getParameter("eventId"));

		//eventIdをeventインスタンスにセット
		event.setEventId(eventId);

		//err変数に0を入れとく
		request.setAttribute("err", SAFE);
		//err2変数に0を入れとく
		request.setAttribute("err2", SAFE);

		try {
			//EventDaoImplを使うためにDaoFactoryからeventDaoインスタンスを作成
			EventDao eventDao = DaoFactory.createEventDao();

			//RoomListDaoImplを使うためにDaoFactoryからroomListDaoインスタンスを作成
			RoomDao roomDao = DaoFactory.createRoomDao();

			//開催場所の項目のための情報(会議室）を取得、転送
			List<Room> rooms = roomDao.findAll();
			request.setAttribute("rooms", rooms);

			//GroupListDaoImplを使うためにDaoFactoryからgroupListDaoインスタンスを作成
			GroupListDao groupListDao = DaoFactory.createGroupListDao();

			//開催場所の項目のための情報（所属部署）を取得、転送
			List<Group> groups = groupListDao.findAll();
			request.setAttribute("groups", groups);

			//findByEventIdで選択されたeventの情報を取得
			event = eventDao.findByEventId(event);

			//eventの情報を"event"でセット
			request.setAttribute("event", event);

			request.getRequestDispatcher("WEB-INF/view/eventEdit.jsp").forward(request, response);//eventEdit.jspに行く

			//例外処理
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//err変数に0を入れとく
		request.setAttribute("err", SAFE);
		//err2変数に0を入れとく
		request.setAttribute("err2", SAFE);
		try {
			//roomListDaoインスタンスの作成
			RoomDao roomDao = DaoFactory.createRoomDao();

			//開催場所の項目のための情報を取得、転送
			List<Room> rooms = roomDao.findAll();
			request.setAttribute("rooms", rooms);

			//groupListDaoインスタンスの作成
			GroupListDao groupListDao = DaoFactory.createGroupListDao();

			//開催場所の項目のための情報を取得、転送
			List<Group> groups = groupListDao.findAll();
			request.setAttribute("groups", groups);

		} catch (Exception e1) {
			e1.printStackTrace();
		}

		//getParameterで値を取得
		Integer eventId = Integer.parseInt(request.getParameter("eventId"));
		String title = request.getParameter("title").trim();
		String start = request.getParameter("start").trim();
		String end = request.getParameter("end").trim();
		int room = Integer.parseInt(request.getParameter("roomId"));
		String detail = request.getParameter("detail").trim();
		Integer groupId = Integer.parseInt(request.getParameter("eventsGroupId"));
		Integer registeredById = Integer.parseInt(request.getParameter("registeredById"));

		//eventインスタンス作成
		Event event = new Event();

		//値をeventにセット
		event.setEventId(eventId);
		event.setTitle(title);
		//Dateインスタンスのdate1を作成
		Date date1 = new Date();
		//Dateインスタンスのdate2を作成
		Date date2 = new Date();
		event.setRoomId(room);
		event.setDetail(detail);
		event.setGroupId(groupId);
		event.setRegisteredById(registeredById);

		//正しい時間かチェック
		Integer err = TimeEr.checkTimeFmt(start);
		Integer err2 = TimeEr.checkTimeFmt(end);

		if (err != SAFE || err2 != SAFE) {
			EventDao eventDao = DaoFactory.createEventDao();
			Event event2 = null;
			try {
				event2 = eventDao.findByEventId(event);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (err != SAFE) {
				//toolsのTimeEr.javaからインスタンスを作成してセット
				request.setAttribute("err", TimeEr.checkTimeFmt(start));
			}
			if (err2 != SAFE) {
				request.setAttribute("err2", TimeEr.checkTimeFmt(end));
			}
			//event情報を取得
			request.setAttribute("event", event2);
			//event2の値をeventEdit.jspに送る
			request.getRequestDispatcher("WEB-INF/view/eventEdit.jsp").forward(request, response);
		} else {

			try {
				//時刻がエラーでなければsetする
				SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				date1 = sdFormat.parse(start);
				event.setStart(date1);
				date2 = sdFormat.parse(end);
				event.setEnd(date2);

			} catch (ParseException e2) {
				e2.printStackTrace();
			}

			//管理者ではなかった場合、過去時間エラーメッセージ
			if ((Integer) request.getSession().getAttribute("loginUserType") != 1) {
				Date now = new Date();

				err = TimeEr.checkPastTime(now, date1);
				err2 = TimeEr.checkPastTime(now, date2);
				Integer err3 = TimeEr.checkPastTime(date1, date2);

				if (err != SAFE || err2 != SAFE || err3 != SAFE) {

					if (err3 != SAFE) {
						request.setAttribute("err2", START_BEFORE_END_ERROR);
					} else if ((err != SAFE) || (err2 != SAFE)) {

						if (err != SAFE) {
							request.setAttribute("err", TIME_PAST_ERROR);
							if (err2 != SAFE) {
								request.setAttribute("err2", TIME_PAST_ERROR);
							}
						}
					}
					request.setAttribute("event", event);
					request.getRequestDispatcher("WEB-INF/view/eventEdit.jsp").forward(request, response);
				} else {
					try {
						EventDao eventDao = DaoFactory.createEventDao();

						//変更前event情報取得
						Event beforeEV = eventDao.findByEventId(event);

						//イベント情報の更新
						eventDao.update(event);

						//変更後event情報取得
						Event afterEV = eventDao.findByEventId(event);

						//groupId変更されてた場合
						if (!(beforeEV.getGroupId() == afterEV.getGroupId())) {

							//attendインスタンス作成
							Attend attend = new Attend();

							//attendインスタンスに変更前EventIdをセット
							attend.setAttendEventId(beforeEV.getEventId());

							AttendDao attendDao = DaoFactory.createAttendDao();

							//変更前eventIdをattendテーブルから削除
							attendDao.attendDeleteEventId(attend);
						}

						request.getRequestDispatcher("WEB-INF/view/eventEditComp.jsp").forward(request, response);

					} catch (Exception e) {
						throw new ServletException(e);
					}
				}
				//管理者だった場合
			} else {
				//startよりendが早い時間だった場合
				err2 = TimeEr.checkPastTime(date1, date2);

				if (err2 != SAFE) {
					EventDao eventDao = DaoFactory.createEventDao();
					try {
						//event情報を取得
						eventDao.findByEventId(event);
					} catch (Exception e) {
						e.printStackTrace();
					}
					request.setAttribute("err2", START_BEFORE_END_ERROR);
					request.setAttribute("event", event);
					request.getRequestDispatcher("WEB-INF/view/eventEdit.jsp").forward(request, response);
				} else {
					try {
						EventDao eventDao = DaoFactory.createEventDao();

						//変更前event情報取得
						Event beforeEV = eventDao.findByEventId(event);

						//イベント情報の更新
						eventDao.update(event);

						//変更後event情報取得
						Event afterEV = eventDao.findByEventId(event);

						//groupId変更されてた場合
						if (!(beforeEV.getGroupId() == afterEV.getGroupId())) {

							Attend attend = new Attend();

							//attendインスタンスに変更前EventIdをセット
							attend.setAttendEventId(beforeEV.getEventId());

							AttendDao attendDao = DaoFactory.createAttendDao();
							//変更前eventIdをattendテーブルから削除
							attendDao.attendDeleteEventId(attend);
						}
						Event event2 = new Event();
						event2.setEventId(eventId);

						//変更後event情報取得
						eventDao.findByEventId(event2);

						//値をセット
						request.setAttribute("event", event2);

						request.getRequestDispatcher("WEB-INF/view/eventEditComp.jsp").forward(request, response);

					} catch (Exception e) {
						throw new ServletException(e);
					}

				}
			}
		}
	}
}