package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.UserDao;
import domain.User;

/**
 * Servlet implementation class UserEditServlet
 */
@WebServlet("/UserEditServlet")
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserEditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//userIdの値を取得
		Integer userId = Integer.valueOf(request.getParameter("userId"));
		int err = 0;
		request.setAttribute("err", err);
		try {
			UserDao userDao = DaoFactory.createUserDao();
			//userにuserIdから取得したusersのそれぞれの値をセット
			request.setAttribute("user", userDao.findByUserId(userId));
			//user編集画面へ
			request.getRequestDispatcher("WEB-INF/view/userEdit.jsp").forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Integer userId = Integer.parseInt(request.getParameter("userId"));
		Integer typeId = Integer.parseInt(request.getParameter("typeId"));

		//エラー表示用の変数初期化
		int err = 0;
		request.setAttribute("err", err);

		UserDao userDao = DaoFactory.createUserDao();
		//passwordがnullの場合、変更前のデータを引き続き使用
		String loginPass = null;
		//新しく入力されてた場合はisEmptyで0にはならないためif文に入る
		if (!request.getParameter("loginPass").isEmpty()) {
			try {
				//新しく受け取ったloginpass値をハッシュ化
				loginPass = userDao.makePasswordHash(request.getParameter("loginPass"));

			} catch (Exception e2) {
				throw new ServletException(e2);
			}
		}
		User userG = null;
		try {
			userG = userDao.findByUserId(userId);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		User user = new User();
		user.setUserId(userId);
		user.setTypeId(typeId);
		try {
			if (loginPass != null) {
				//新しいpassセット
				user.setPass(loginPass);
			}
		} catch (Exception e) {
			throw new ServletException(e);
		}
		try {
			userDao.update(user);
			request.getRequestDispatcher("WEB-INF/view/userEditComp.jsp").forward(request, response);
		} catch (Exception e) {
			System.out.println(e);
			throw new ServletException(e);
		}
	}
}
