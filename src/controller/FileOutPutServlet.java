package controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendDao;
import dao.DaoFactory;
import dao.EventSearchDao;
import dao.FileOutPutDao;
import dao.GroupListDao;
import dao.RoomDao;
import domain.Attend;
import domain.Event;
import domain.EventForFileOutPut;
import domain.Group;
import domain.Room;
import tools.Page;
import util.FileOutPut;
import util.OutPutPropertiesGetter;

/**
 * Servlet implementation class FileOutPutServlet
 */
@WebServlet("/FileOutPutServlet")
public class FileOutPutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FileOutPutServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//絞り込みを行う所属グループのID
		int groupID = Integer.parseInt(request.getParameter("groupId"));

		//絞り込みを行う会議室のID
		int roomID = Integer.parseInt(request.getParameter("roomId"));

		//DaoFactoryで FileOutPutDaoのインスタンスを作成
		FileOutPutDao FileOutPutDao = DaoFactory.createFileOutPutDao();

		//CSVファイルの出力先をfile_out_put.propertiesから取得
		ServletContext context = this.getServletContext();
		String path = context.getRealPath("/WEB-INF/classes/file_out_put.properties");
		OutPutPropertiesGetter p = new OutPutPropertiesGetter();
		String outPutPath = null;
		try {
			outPutPath = p.getProperties(path);
		} catch (IOException e) {
			//******プロパティファイルが読み込めなかった時の処理開始******
			request.setAttribute("notExistPropertieError", true);
			try {
				processingWhenErrer(groupID, roomID, request);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			request.getRequestDispatcher("WEB-INF/view/eventManageSearch.jsp").forward(request,
					response);
			return;
			//******プロパティファイルが読み込めなかった時の処理終了******
		}

		//上書きチェックをするかどうかの判定
		boolean overRideCheckFlug = Boolean.valueOf(request.getParameter("overRideCheckFlug"));
		if (overRideCheckFlug) {

			//csvファイル名の日付部分の準備
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date dt1 = new Date();
			File file = new File(outPutPath + "/events_" + sdf.format(dt1) + ".csv");

			if (file.exists()) {
				//**********すでにCSVファイルが存在していた場合の処理開始**********
				try {
					request.setAttribute("overRideError", true);
					processingWhenErrer(groupID, roomID, request);
					request.getRequestDispatcher("WEB-INF/view/eventManageSearch.jsp").forward(request,
							response);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return;
				//**********すでにCSVファイルが存在していた場合の処理終了**********
			}

		}

		try {
			//groupID、roomIDに対応する所属名、会議室名を取得。
			//idが0に対応するものは無いため、その時は"指定なし"とする。
			String groupName = null;
			String roomName = null;
			if (groupID == 0) {
				groupName = "指定なし";
			} else {
				groupName = FileOutPutDao.getGroupNmaeByGroupID(groupID);
			}
			if (roomID == 0) {
				roomName = "指定なし";
			} else {
				roomName = FileOutPutDao.getRoomNameByRoomID(roomID);
			}

			//CSVファイルに出力するためのイベントリストを作成
			List<EventForFileOutPut> FileOutPutList = FileOutPutDao.getEventListByGroupNameRoomName(groupID, roomID);

			//CSVファイルを出力
			FileOutPut fop = new FileOutPut();
			fop.fileOutPut(groupName, roomName, FileOutPutList, outPutPath);

			response.sendRedirect("EventManageServlet");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * すでにCSVファイルが存在していた場合の処理を行うメソッド。
	 * 上書き確認を行うページに表示する情報を設定する。
	 * doPostメソッドの中に記述してもよいのだが、コードが冗長になるため別のメソッドにした。
	 * @param groupID
	 * 絞り込みを行う所属ID
	 * @param roomID
	 * 絞り込みを行う会議室ID
	 * @param request
	 * リクエストオブジェクト
	 * @author R_kotaki
	 */
	private void processingWhenErrer(int groupID, int roomID, HttpServletRequest request)
			throws Exception {

		//ログインしているユーザのログインIDを取得
		String loginId = (String) request.getSession().getAttribute("loginId");

		request.setAttribute("groupID", groupID);
		request.setAttribute("roomID", roomID);

		AttendDao AttendDao = DaoFactory.createAttendDao();
		EventSearchDao EventSearchDao = DaoFactory.createEventSearchDao();

		//全ページ数を計算する。
		int numberOfTableRow = EventSearchDao.getNumberOfTableRow(groupID, roomID);
		int totalNumberOfPages = Page.allPageNumber(numberOfTableRow);
		request.setAttribute("totalNumberOfPages", totalNumberOfPages);

		//表示するページ番号を設定
		int currentPageNumber = 1;
		request.setAttribute("currentPageNumber", currentPageNumber);
		//loginしたユーザが参加するイベントをリストにする
		List<Attend> attendList = AttendDao.findByLoginId(loginId);

		//イベントの一覧と、ログインしているユーザが参加するイベントのリストをEventListにセット
		List<Event> EventList = EventSearchDao.search(groupID, roomID, currentPageNumber, attendList);

		//roomListDaoインスタンスの作成
		RoomDao roomDao = DaoFactory.createRoomDao();

		//開催場所の項目のための情報を取得、転送
		List<Room> rooms = roomDao.findAll();
		request.setAttribute("rooms", rooms);

		//groupListDaoインスタンスの作成
		GroupListDao groupListDao = DaoFactory.createGroupListDao();

		//開催場所の項目のための情報を取得、転送
		List<Group> groups = groupListDao.findAll();
		request.setAttribute("groups", groups);

		request.setAttribute("EventList", EventList);

	}

}
