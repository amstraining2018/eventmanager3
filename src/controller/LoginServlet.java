package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.LoginDao;
import domain.Login;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("error", false);
		request.getRequestDispatcher("WEB-INF/view/login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//      パラメータの取得

		String loginId = request.getParameter("loginId");
		String loginPass = request.getParameter("loginPass");

		try {
			LoginDao logDao = DaoFactory.createLoginDao();
			Login login = logDao.findByLoginIdAndLoginPass(loginId, loginPass);

			//		ログインができていれば下の処理を行う
			if (login != null) {
				request.getSession().setAttribute("loginId", login.getLoginId());
				request.getSession().setAttribute("loginUserName", login.getUserName());
				request.getSession().setAttribute("loginUserType", login.getTypeId());
				request.getSession().setAttribute("loginUserId", login.getUserId());
				request.getSession().setAttribute("loginGroupId", login.getGroupId());
				response.sendRedirect("TodaysEventServlet");

			} else {
				request.setAttribute("error", true);
				request.getRequestDispatcher("WEB-INF/view/login.jsp").forward(request, response);
			}

		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

}
