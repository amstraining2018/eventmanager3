﻿package controller;

import static tools.Constants.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.EventDao;
import dao.GroupListDao;
import dao.RoomDao;
import domain.Event;
import domain.Group;
import domain.Room;
import tools.TimeEr;

/**
 * Servlet implementation class EventRegistServlet
 */
@WebServlet("/EventRegistServlet")
public class EventRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventRegistServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//		エラーメッセージ初期設定
		request.setAttribute("err", SAFE);
		request.setAttribute("err2", SAFE);

		try {
			//roomListDaoインスタンスの作成
			RoomDao roomDao = DaoFactory.createRoomDao();

			//開催場所の項目のための情報を取得、転送
			List<Room> rooms = roomDao.findAll();
			request.setAttribute("rooms", rooms);

			//groupListDaoインスタンスの作成
			GroupListDao groupListDao = DaoFactory.createGroupListDao();

			//開催場所の項目のための情報を取得、転送
			List<Group> groups = groupListDao.findAll();
			request.setAttribute("groups", groups);

		} catch (Exception e) {
			throw new ServletException();
		}

		//登録画面に遷移
		request.getRequestDispatcher("WEB-INF/view/eventRegist.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("err", SAFE);
		request.setAttribute("err2", SAFE);
		try {
			//roomListDaoインスタンスの作成
			RoomDao roomDao = DaoFactory.createRoomDao();

			//開催場所の項目のための情報を取得、転送
			List<Room> rooms = roomDao.findAll();
			request.setAttribute("rooms", rooms);

			//groupListDaoインスタンスの作成
			GroupListDao groupListDao = DaoFactory.createGroupListDao();

			//開催場所の項目のための情報を取得、転送
			List<Group> groups = groupListDao.findAll();
			request.setAttribute("groups", groups);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		//jspで入力した値を取得する	※trimは空白削除用のもの
		String title = request.getParameter("title").trim();
		String start = request.getParameter("start").trim();
		String end = request.getParameter("end").trim();
		int room = Integer.parseInt(request.getParameter("roomId"));
		String detail = request.getParameter("detail").trim();
		Integer userId = (Integer) request.getSession().getAttribute("loginUserId");
		Integer groupId = Integer.parseInt(request.getParameter("eventsGroupId"));

		Event event = new Event();
		event.setTitle(title);
		Date date1 = new Date();
		Date date2 = new Date();
		event.setRoomId(room);
		event.setUserId(userId);
		event.setDetail(detail);
		event.setGroupId(groupId);

		//		不正な時間かを検出するメソッドを使ってstartとendを確認する
		Integer err = TimeEr.checkTimeFmt(start);
		Integer err2 = TimeEr.checkTimeFmt(end);
		if (err != SAFE || err2 != SAFE) {
			if (err != SAFE) {
				request.setAttribute("err", TIME_FORMAT_ERROR);
			}
			if (err2 != SAFE) {
				request.setAttribute("err2", TIME_FORMAT_ERROR);
			}
			request.getRequestDispatcher("WEB-INF/view/eventRegist.jsp").forward(request, response);
		} else {

			try {

				//			時刻がエラーでなければsetする
				SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				date1 = sdFormat.parse(start);
				event.setStart(date1);
				date2 = sdFormat.parse(end);
				event.setEnd(date2);

			} catch (ParseException e2) {
				e2.printStackTrace();
			}
			//現在時刻を取得
			Date now = new Date();
			err = TimeEr.checkPastTime(now, date1);
			err2 = TimeEr.checkPastTime(now, date2);
			Integer err3 = TimeEr.checkPastTime(date1, date2);

			if (err != SAFE || err2 != SAFE || err3 != SAFE) {

				if (err3 != SAFE) {
					request.setAttribute("err2", START_BEFORE_END_ERROR);
				} else if ((err != SAFE) || (err2 != SAFE)) {

					if (err != SAFE) {
						request.setAttribute("err", TIME_PAST_ERROR);
						if (err2 != SAFE) {
							request.setAttribute("err2", TIME_PAST_ERROR);
						}
					}
				}
				request.getRequestDispatcher("WEB-INF/view/eventRegist.jsp").forward(request, response);
			} else {

				try {
					EventDao EventDao = DaoFactory.createEventDao();
					//				全部のエラーがなければ挿入する
					EventDao.insert(event);
					//				完了画面jspに飛ぶ
					request.getRequestDispatcher("WEB-INF/view/eventRegistComp.jsp").forward(request, response);
				} catch (Exception e) {
					throw new ServletException(e);

				}
			}
		}
	}
}
