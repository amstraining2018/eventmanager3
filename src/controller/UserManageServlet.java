package controller;

import static tools.Constants.SAFE;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.UserDao;
import domain.User;
import tools.Page;

/**
 * Servlet implementation class UserManageServlet
 */
@WebServlet("/UserManageServlet")
public class UserManageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserManageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//エラーメッセージの初期値
		request.setAttribute("err", SAFE);
		if (request.getAttribute("errerr") != null) {
			Integer err = (Integer) request.getAttribute("errerr");
			request.setAttribute("err", err);
		}
		//まずはnullだからif文に入らずelse以降の処理を行う(最初だけ1ページ目を表示させる処理を行う)
		String id = null;
		String pg = null;

		try {
			UserDao UserDao = DaoFactory.createUserDao();
			//jspから戻ってくるときにidとpgの値を持ってくるため2回目以降nullでなくなりif文に入る
			if (request.getParameter("id") != null & request.getParameter("pg") != null) {
				id = request.getParameter("id");
				pg = request.getParameter("pg");

				//pgをページ数としてセットする
				Integer PageCount = null;
				PageCount = Integer.parseInt(pg);
				//ユーザの一覧をリスト化
				List<User> UserList = UserDao.findAll(PageCount);

				//ユーザ一覧リストをセット
				request.setAttribute("UserList", UserList);
				//jspにページ番号とページ数を送る
				request.getRequestDispatcher("WEB-INF/view/userManage.jsp?id=" + id + "&page=" + pg).forward(request,
						response);

			} else {
				//行数を取得してページを振り分ける
				Integer r = UserDao.countRow();

				//全ページ数
				Integer allPageNumber = Page.allPageNumber(r);

				//1ページ目の表示
				Integer PageCount = null;
				PageCount = 1;
				List<User> UserList = UserDao.findAll(PageCount);

				//ユーザ一覧リストをセット
				request.setAttribute("UserList", UserList);
				//jspにページ番号とページ数を送る
				request.getRequestDispatcher("WEB-INF/view/userManage.jsp?id=" + allPageNumber + "&page=1").forward(
						request, response);
			}
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
}
