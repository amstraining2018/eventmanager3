package controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendDao;
import dao.DaoFactory;
import dao.EventDao;
import domain.Attend;
import domain.Event;

/**
 * Servlet implementation class EventDetailInsert
 */
@WebServlet("/EventDetailInsert")
public class EventDetailInsert extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventDetailInsert() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**プッシュ用
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//		int eventId = Integer.parseInt(request.getParameter("eventId"));
		//		int userId = 1;

		//ログイン者のユーザidを保持
		Integer userId = (Integer) request.getSession().getAttribute("loginUserId");

		//選択したイベントのid
		Integer eventId = Integer.parseInt(request.getParameter("eventId"));

		//参加ボタン、編集削除ボタンを表示するか否か
		boolean join = false;
		try {

			//DaoFactoryで eventDaoのインスタンスを作成
			EventDao eventDao = DaoFactory.createEventDao();

			//DaoFactoryで attendDaoのインスタンスを作成
			AttendDao attendDao = DaoFactory.createAttendDao();

			//EventとAttendのインスタンス
			Event event = new Event();
			Attend attend = new Attend();

			//インスタンスのeventにeventIdをセット
			event.setEventId(eventId);

			//インスタンスのattendにeventId、userIdをセット
			attend.setAttendEventId(eventId);
			attend.setAttendUserId(userId);

			//インスタンスのattendDaoにinsert(attend)の返り値を代入
			//参加ボタン
			attendDao.insert(attend);

			//EventDaoImplからfindAttendName(event)の返り値を代入
			//参加者を表示
			List<Event> eventList = eventDao.findAttendName(event);

			//	EventDaoImplからfindByEventId(event)の返り値を代入
			//イベントid、イベントタイトル、開始時間、終了時間、場所、所属、詳細、登録者を表示
			event = eventDao.findByEventId(event);

			//AttendDaoImplから参加しているかしていないかの判断
			//joining(attend)の返り値を代入
			join = attendDao.joining(attend);

			//カレンダークラス、現在の日時を取得
			Calendar now = Calendar.getInstance();
			//取得した開始日時が未来か否か
			boolean ifbefore = !event.getStart().before(now.getTime());

			//それぞれの変数に入る値をセットする
			request.setAttribute("ifbefore", ifbefore);
			request.setAttribute("event", event);
			request.setAttribute("eventList", eventList);
			request.setAttribute("join", join);
			request.setAttribute("userId", userId);
			request.setAttribute("eventId", eventId);

			//request, responseを受け取ってview/eventDetail.jspに遷移
			request.getRequestDispatcher("WEB-INF/view/eventDetail.jsp").forward(request, response);

			//例外処理
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

}
