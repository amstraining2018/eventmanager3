package controller;

import static tools.Constants.*;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendDao;
import dao.DaoFactory;
import dao.EventDao;
import dao.GroupListDao;
import dao.RoomDao;
import domain.Attend;
import domain.Event;
import domain.Group;
import domain.Room;
import tools.Page;

/**
 * Servlet implementation class EventManageServlet
 */
@WebServlet("/EventManageServlet")
public class EventManageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//エラーメッセージの初期値
		request.setAttribute("err", SAFE);
		if (request.getAttribute("errerr") != null) {
			Integer err = (Integer) request.getAttribute("errerr");
			request.setAttribute("err", err);
		}
		//まずはnullだからif文に入らずelse以降の処理を行う(最初だけ1ページ目を表示させる処理を行う)
		String id = null;
		String pg = null;

		try {
			AttendDao AttendDao = DaoFactory.createAttendDao();
			String loginId = (String) request.getSession().getAttribute("loginId");
			List<Attend> attendEventList = AttendDao.findByLoginId(loginId);//loginしたユーザが参加するイベントをリストにしている(id.名前.ユーザID.イベントID)

			//roomListDaoインスタンスの作成
			RoomDao roomDao = DaoFactory.createRoomDao();

			//開催場所の項目のための情報を取得、転送
			List<Room> rooms = roomDao.findAll();
			request.setAttribute("rooms", rooms);

			//groupListDaoインスタンスの作成
			GroupListDao groupListDao = DaoFactory.createGroupListDao();

			//開催場所の項目のための情報を取得、転送
			List<Group> groups = groupListDao.findAll();
			request.setAttribute("groups", groups);

			EventDao EventDao = DaoFactory.createEventDao();
			//jspから戻ってくるときにidとpgの値を持ってくるため2回目以降nullでなくなりif文に入る
			if (request.getParameter("id") != null & request.getParameter("pg") != null) {
				id = request.getParameter("id");
				pg = request.getParameter("pg");
				//pgをページ数としてセットする
				Integer PageCount = null;
				PageCount = Integer.parseInt(pg);
				//イベントの一覧と、ログインしているユーザが参加するイベントのリストをEventListにセット
				List<Event> EventList = EventDao.findAll(PageCount, attendEventList);

				//event(表の中身), attendEventList(参加するか否か)をEventListに持たせる
				request.setAttribute("EventList", EventList);
				request.getRequestDispatcher("WEB-INF/view/eventManage.jsp?id=" + id + "&page=" + pg).forward(request,
						response);//jspにページ番号とページ数を送る

			} else {
				//データベース内のeventテーブルの行数を取得
				Integer r = EventDao.countRow();

				//全ページ数
				Integer allPageNumber = Page.allPageNumber(r);

				//1ページ目の情報を取得
				Integer PageCount = null;
				PageCount = 1;
				List<Event> EventList = EventDao.findAll(PageCount, attendEventList);

				//event(表の中身), attendEventList(参加するか否か)をEventListに持たせる
				request.setAttribute("EventList", EventList);
				//jspにページ番号とページ数を送る
				request.getRequestDispatcher("WEB-INF/view/eventManage.jsp?id=" + allPageNumber + "&page=1").forward(
						request,
						response);
			}

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
}
