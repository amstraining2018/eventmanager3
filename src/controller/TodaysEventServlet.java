package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendDao;
import dao.DaoFactory;
import dao.EventDao;
import domain.Attend;
import domain.Event;
import tools.Page;

/**
 * Servlet implementation class TodaysEventServlet
 */
@WebServlet({ "/TodaysEventServlet", "/todaysEvent" })
public class TodaysEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TodaysEventServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//まずはnullだからif文に入らず66行目以降の処理を行う(最初だけ1ページ目を表示させる処理を行う)
		//id→ページ数、pg→現在のページ番号
		String id = null;
		String pg = null;

		try {
			//参加マークを表示させる
			AttendDao AttendDao = DaoFactory.createAttendDao();
			//String loginUserId = (String) request.getSession().getAttribute("loginId");
			String loginId = (String) request.getSession().getAttribute("loginId");
			//ログインしたユーザが参加するイベントをリストにしている(id.名前.ユーザID.イベントID)
			List<Attend> attendEventList = AttendDao.findByLoginId(loginId);

			EventDao EventDao = DaoFactory.createEventDao();
			//jspから戻ってくるときにidとpgの値を持ってくるため2回目以降nullでなくなりif文に入る
			if (request.getParameter("id") != null & request.getParameter("pg") != null) {
				id = request.getParameter("id");
				pg = request.getParameter("pg");
				Integer PageCount = null;
				//pgをページ数としてセットする
				PageCount = Integer.parseInt(pg);
				//今日のイベントの一覧と、ログインしているユーザが参加するイベントのリストをEventListにセット
				List<Event> EventList = EventDao.findByToday(PageCount, attendEventList);

				//event(表の中身), attendEventList(参加するか否か)をEventListに持たせる
				request.setAttribute("eventList", EventList);
				request.getRequestDispatcher("WEB-INF/view/todaysEvent.jsp?id=" + id + "&page=" + pg).forward(request,
						response);//jspにページ番号とページ数を送る

			} else {
				//データベース内のeventテーブルの行数を取得
				Integer r = EventDao.countToday();

				//全ページ数
				Integer allPageNumber = Page.allPageNumber(r);

				//1ページ目の情報を取得
				Integer PageCount = null;
				PageCount = 1;//1ページ目
				List<Event> EventList = EventDao.findByToday(PageCount, attendEventList);

				//EventListを表示するページに送る
				request.setAttribute("eventList", EventList);

				//jspにページ番号とページ数を送る
				request.getRequestDispatcher("WEB-INF/view/todaysEvent.jsp?id=" + allPageNumber + "&page=1").forward(
						request,
						response);
			}

		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
