package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.UserDao;
import dao.UserTypesDao;
import domain.User;
import tools.Constants;
import tools.Constants2;
import tools.DuplicateCheck;
import tools.UserFileCheck;
import tools.UserRecordCheck;
import util.PropertiesGetter;

/**
 * Servlet implementation class UserBatchRegistServlet
 */
@WebServlet({ "/UserBatchRegistServlet", "/userBatchRegist" })
public class UserBatchRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PATH = "/WEB-INF/classes/file_ja.properties";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserBatchRegistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServletContext context = this.getServletContext();
		String path = context.getRealPath(PATH);
		PropertiesGetter p = new PropertiesGetter();
		//ユーザ一括登録用のCSVファイルを開く
		String dir = p.getProperties(PropertiesGetter.USER, path);
		File file = new File(dir);
		//ファイルが存在しているかチェック
		if (!file.exists()) {
			//エラーがあることを数値-3で返却し、request.getRequestDispatcherで戻る処理
			request.setAttribute("errerr", Constants.NO_FILE_ERROR);
		} else {
			//ファイルが存在していれば下記を実行する
			FileInputStream fi = null;
			InputStreamReader is = null;
			BufferedReader br = null;
			try {
				boolean check = false;
				boolean empcheck = false;
				boolean logcheck = false;

				//ファイルチェックメソッド
				check = UserFileCheck.check(dir);
				if (!check) {
					//エラーがあることを数値-4で返却し、request.getRequestDispatcherで戻る処理
					request.setAttribute("errerr", Constants.FILE_CONTENTS_ERROR);
				} else {

					//ファイルチェックが正しかった場合
					//チェックでokだった場合のみ下記を実行するようにする
					//レコードチェックメソッド
					check = UserRecordCheck.check(dir);
					if (!check) {
						//エラーがあることを数値-5で返却し、request.getRequestDispatcherで戻る処理
						request.setAttribute("errerr", Constants.RECORD_CONTENTS_ERROR);
					} else {

						// ファイル内でemployee_idの重複がないかチェック
						empcheck = DuplicateCheck.check(dir, Constants2.EMPLOYEE_ID);
						if (!empcheck) {
							//エラーがあることを数値-6で返却し、request.getRequestDispatcherで戻る処理
							request.setAttribute("errerr", Constants.RECORD_CONTENTS_DUPLICATE);
						} else {

							// ファイル内でlogin_idの重複がないかチェック
							logcheck = DuplicateCheck.check(dir, Constants2.LOGIN_ID);
							if (!logcheck) { //エラーがあることを数値-6で返却し、request.getRequestDispatcherで戻る処理
								request.setAttribute("errerr", Constants.RECORD_CONTENTS_DUPLICATE);
							} else {

								//レコードチェックが正しかった場合
								//チェックでokだった場合のみ下記を実行するようにする
								String line;
								boolean ret = false;
								boolean ret1 = false;
								boolean ret2 = false;
								String login_pass;
								UserDao userDao = DaoFactory.createUserDao();
								UserTypesDao userTypesDao = DaoFactory.createUser_TypesDao();
								fi = new FileInputStream(dir);
								is = new InputStreamReader(fi, Constants2.SHIFT_JIS);
								br = new BufferedReader(is);

								//1行ずつデータのある分だけまわす
								while ((line = br.readLine()) != null) {
									//各行をカンマ区切りして配列にする
									String[] data = line.split(Constants2.COMMA, -1);
									String top = data[0];
									//配列の0番目がDの時(データ行)の中身をチェックする
									if (top.equals(Constants2.DATA)) {
										String employee_id = data[Constants2.EMPLOYEE_ID];
										String login_id = data[Constants2.LOGIN_ID];
										Integer typeId = Integer.parseInt(data[Constants2.TYPE_ID]);

										//nameにかぶりがないかチェック
										ret = userDao.checkExsist(Constants2.EMPLOYEE, employee_id);
										if (!ret) {
											break;
										}
										ret1 = userDao.checkExsist(Constants2.LOGIN, login_id);
										if (!ret1) {
											break;
										}
										ret2 = userTypesDao.checkExsist(typeId);
										if (!ret2) {
											break;
										}
									}
								}
								if (!ret || !ret1) {
									request.setAttribute("errerr", Constants.RECORD_DUPLICATE);
								} else if (!ret2) {
									request.setAttribute("errerr", Constants.RECORD_CONTENTS_DUPLICATE);
								} else {

									fi = new FileInputStream(dir);
									is = new InputStreamReader(fi, Constants2.SHIFT_JIS);
									br = new BufferedReader(is);

									while ((line = br.readLine()) != null) {
										//各行をカンマ区切りして配列にする
										String[] data = line.split(Constants2.COMMA, -1);
										String top = data[Constants2.SHDE];
										//配列の0番目がDの時(データ行)の中身をチェックする
										if (top.equals(Constants2.DATA)) {
											String employee_id = data[Constants2.EMPLOYEE_ID];
											String login_id = data[Constants2.LOGIN_ID];
											String pass = data[Constants2.PASS];
											Integer typeId = Integer.parseInt(data[Constants2.TYPE_ID]);

											//	入力されたパスワード(pass)をハッシュ化するメソッド
											login_pass = userDao.makePasswordHash(pass);

											User user = new User();
											user.setEmployee_id(employee_id);
											user.setLogin_id(login_id);
											user.setPass(login_pass);
											user.setTypeId(typeId);

											userDao.insert(user);

										}
									}
									//エラーがないことを数値0で返却
									request.setAttribute("errerr", Constants.SAFE);
								}
							}
						}
					}
				}
			} catch (IOException e) {
				request.setAttribute("errerr", Constants.FILE_OPEN_ERROR);
			} catch (SQLException e) {
				request.setAttribute("errerr", Constants.INSERT_ERROR);
			} finally {
				try {
					br.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		//元のページに戻る
		//		request.getRequestDispatcher("WEB-INF/view/userManage.jsp").forward(request, response);
		ServletContext sc = getServletContext();
		RequestDispatcher dispatcher = sc.getRequestDispatcher("/UserManageServlet");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
