package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendDao;
import dao.DaoFactory;
import dao.EventDao;
import domain.Attend;
import domain.Event;

/**
 * Servlet implementation class EventDeleteComp
 */
@WebServlet("/EventDeleteComp")
public class EventDeleteComp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventDeleteComp() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//送られてきたeventIdを取得
		int id = Integer.parseInt(request.getParameter("eventId"));
		try {
			AttendDao attendDao = DaoFactory.createAttendDao();
			EventDao eventDao = DaoFactory.createEventDao();

			Event event = new Event();
			Attend attend = new Attend();
			//インスタンスにセット
			event.setEventId(id);
			//インスタンスにセット
			attend.setAttendEventId(id);
			//そのeventIdの行を削除
			eventDao.delete(event);
			//attendテーブルからイベントを削除
			attendDao.attendDeleteEventId(attend);

			request.getRequestDispatcher("WEB-INF/view/eventDeleteComp.jsp").forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
}
