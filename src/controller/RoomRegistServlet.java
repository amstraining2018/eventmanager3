package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.RoomDao;
import domain.Room;
import tools.Constants;
import tools.Constants2;
import tools.DuplicateCheck;
import tools.FileCheck;
import tools.RecordCheck;
import util.PropertiesGetter;

/**
 * Servlet implementation class RoomRegistServlet
 */
@WebServlet({ "/RoomRegistServlet", "/roomRegist" })
public class RoomRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PATH = "/WEB-INF/classes/file_ja.properties";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RoomRegistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ServletContext context = this.getServletContext();
		String path = context.getRealPath(PATH);
		PropertiesGetter p = new PropertiesGetter();
		//会場一括登録用のCSVファイルを開く
		String dir = p.getProperties(PropertiesGetter.ROOM, path);
		File file = new File(dir);
		//ファイルが存在しているかチェック
		if (!file.exists()) {
			//エラーを返却し、request.getRequestDispatcherで戻る処理
			request.setAttribute("errerr", Constants.NO_FILE_ERROR);
		} else {
			FileInputStream fi = null;
			InputStreamReader is = null;
			BufferedReader br = null;

			try {
				boolean check = false;

				//ファイルチェックメソッド
				check = FileCheck.check(dir);
				if (!check) {
					//エラーがあることを数値-4で返却し、request.getRequestDispatcherで戻る処理
					request.setAttribute("errerr", Constants.FILE_CONTENTS_ERROR);
				} else {
					//チェックでokだった場合のみ下記を実行するようにする

					//レコードチェックメソッド
					check = RecordCheck.check(dir);
					if (!check) {
						//エラーがあることを数値-5で返却し、request.getRequestDispatcherで戻る処理
						request.setAttribute("errerr", Constants.RECORD_CONTENTS_ERROR);
					} else {

						// ファイル内での重複がないかチェック
						check = DuplicateCheck.check(dir, Constants2.NAME);
						if (!check) {
							//エラーがあることを数値-6で返却し、request.getRequestDispatcherで戻る処理
							request.setAttribute("errerr", Constants.RECORD_CONTENTS_DUPLICATE);
						} else {

							//レコードチェックが正しかった場合
							//チェックでokだった場合のみ下記を実行するようにする
							String line;
							boolean ret = false;
							RoomDao roomDao = DaoFactory.createRoomDao();

							fi = new FileInputStream(dir);
							is = new InputStreamReader(fi, Constants2.SHIFT_JIS);
							br = new BufferedReader(is);

							//1行ずつデータのある分だけまわす
							while ((line = br.readLine()) != null) {
								//各行をカンマ区切りして配列にする
								String[] data = line.split(Constants2.COMMA, -1);
								String top = data[Constants2.SHDE];
								//配列の0番目がDの時(データ行)の中身をチェックする
								if (top.equals(Constants2.DATA)) {
									String name = data[Constants2.NAME];
									//nameにかぶりがないかチェック
									ret = roomDao.checkExsist(name);
									if (!ret) {
										break;
									}
								}
							}
							if (!ret) {
								request.setAttribute("errerr", Constants.RECORD_DUPLICATE);
							} else {

								fi = new FileInputStream(dir);
								is = new InputStreamReader(fi, Constants2.SHIFT_JIS);
								br = new BufferedReader(is);
								while ((line = br.readLine()) != null) {
									String[] data = line.split(Constants2.COMMA);
									//String[]なので、それぞれの型に変換しroomにセット
									if (data[Constants2.SHDE].equals(Constants2.DATA)) {
										String name = data[Constants2.NAME];
										Integer capacity = Integer.parseInt(data[Constants2.CAPACITY]);
										Integer mic = Integer.parseInt(data[Constants2.MIC]);
										Integer board = Integer.parseInt(data[Constants2.BOARD]);
										Integer projector = Integer.parseInt(data[Constants2.PROJECTOR]);
										String close = data[Constants2.CLOSE];

										Room room = new Room();
										room.setName(name);
										room.setCapacity(capacity);
										room.setMic(mic);
										room.setBoard(board);
										room.setProjector(projector);
										room.setClose(close);

										roomDao.insert(room);
									}
								}
								//エラーがないことを数値0で返却
								request.setAttribute("errerr", Constants.SAFE);
							}
						}
					}
				}
			} catch (IOException e) {
				request.setAttribute("errerr", Constants.FILE_OPEN_ERROR);
			} catch (SQLException e) {
				request.setAttribute("errerr", Constants.INSERT_ERROR);
			} finally {
				try {
					br.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		//元のページに戻る
		//		request.getRequestDispatcher("WEB-INF/view/eventManage.jsp").forward(request, response);
		ServletContext sc = getServletContext();
		RequestDispatcher dispatcher = sc.getRequestDispatcher("/EventManageServlet");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
