package domain;

public class Attend {

	private Integer attendId;

	private String loginId;

	private String attendtf;

	private Integer eventId;

	private Integer attendUserId;

	private Integer attendEventId;

	public Integer getAttendId() {
		return attendId;
	}

	public void setAttendId(Integer attendId) {
		this.attendId = attendId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getAttendtf() {
		return attendtf;
	}

	public void setAttendtf(String attendtf) {
		this.attendtf = attendtf;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Integer getAttendUserId() {
		return attendUserId;
	}

	public void setAttendUserId(Integer attendUserId) {
		this.attendUserId = attendUserId;
	}

	public Integer getAttendEventId() {
		return attendEventId;
	}

	public void setAttendEventId(Integer attendEventId) {
		this.attendEventId = attendEventId;
	}

}
