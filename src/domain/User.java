package domain;

import java.util.Date;

public class User {
	//	テーブルusersのidです。
	private Integer userId;
	//	ユーザの名前です
	private String name;
	//	ユーザのtype_idです。権限関連のものなので、無視して構わないです。
	private Integer typeId;

	private String typeName;
	//	ユーザのgroup_idです
	private Integer groupId;
	//	groupsのname、ユーザのグループを表します。
	private String groupName;
	//	usersのcreatedです。ユーザ情報の作成時間を表します。
	private Date userCreated;
	// ユーザーのpassです。
	private String pass;

	private Integer pageCount;

	private Integer rowCount;

	private String employee_id;

	private String login_id;

	/**
	 * @return employee_id
	 */
	public String getEmployee_id() {
		return employee_id;
	}

	/**
	 * @param employee_id セットする employee_id
	 */
	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public Integer getRowCount() {
		return rowCount;
	}

	public void setRowCount(Integer rowCount) {
		this.rowCount = rowCount;
	}

	/**
	 * @return login_id
	 */
	public String getLogin_id() {
		return login_id;
	}

	/**
	 * @param login_id セットする login_id
	 */
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	/**
	 * @return pageCount
	 */
	public Integer getPageCount() {
		return pageCount;
	}

	/**
	 * @param pageCount セットする pageCount
	 */
	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Date getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(Date userCreated) {
		this.userCreated = userCreated;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

}
