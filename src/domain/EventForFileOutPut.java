package domain;

import java.util.Date;

public class EventForFileOutPut {

	//2019/06/06小滝追加
	//このイベントに参加している人数
	private int numberOfAttendingUser;

	//イベント名です。
	private String title;

	//イベントの開始時間です。
	private Date start;

	//イベントが行われる場所です。
	private String placeName;

	//イベントが対象とする所属グループ
	private String groupName;

	/**
	 * @return numberOfAttendingUser
	 */
	public int getNumberOfAttendingUser() {
		return numberOfAttendingUser;
	}

	/**
	 * @param numberOfAttendingUser セットする numberOfAttendingUser
	 */
	public void setNumberOfAttendingUser(int numberOfAttendingUser) {
		this.numberOfAttendingUser = numberOfAttendingUser;
	}

	/**
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title セットする title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return start
	 */
	public Date getStart() {
		return start;
	}

	/**
	 * @param start セットする start
	 */
	public void setStart(Date start) {
		this.start = start;
	}

	/**
	 * @return placeName
	 */
	public String getPlaceName() {
		return placeName;
	}

	/**
	 * @param placeName セットする placeName
	 */
	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	/**
	 * @return groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName セットする groupName
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}
