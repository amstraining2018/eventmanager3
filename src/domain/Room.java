package domain;

//後で消す
public class Room {
	private Integer id;
	private String name;
	private Integer capacity;
	private Integer mic;
	private Integer board;
	private Integer projector;
	private String close;

	public String getClose() {
		return close;
	}

	public void setClose(String close) {
		this.close = close;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public Integer getMic() {
		return mic;
	}

	public void setMic(Integer mic) {
		this.mic = mic;
	}

	public Integer getBoard() {
		return board;
	}

	public void setBoard(Integer board) {
		this.board = board;
	}

	public Integer getProjector() {
		return projector;
	}

	public void setProjector(Integer projector) {
		this.projector = projector;
	}

}
