package tools;

/**
 * 読み込んだcsvファイルに関する定数クラス<br>
 * 【文字コード、行数、行ごとに格納される際の配列番号】<br>
 */
public class Constants2 {
	private Constants2() {

	}

	/**
	 * 読み込んだcsvファイルに適用する文字コード<br>
	 * 定数名：SHIFT_JIS<br>
	 * 値：Shift_JIS
	 */
	public static final String SHIFT_JIS = "Shift_JIS";

	/**
	 * 読み込んだcsvファイルの1行目<br>
	 * 定数名：ROW_NUM_1<br>
	 * 値：0
	 */
	public static final int START_ROW = 0;

	/**
	 * 読み込んだcsvファイルの2行目<br>
	 * 定数名：ROW_NUM_2<br>
	 * 値：1
	 */
	public static final int ITEM_ROW = 1;

	/**
	 * 読み込んだcsvファイルの各行における1列目のデータ<br>
	 * 定数名：ROW_NAME_START<br>
	 * 値：0
	 */
	public static final int SHDE = 0;
	//会議室用
	public static final int NAME = 1;
	public static final int CAPACITY = 2;
	public static final int MIC = 3;
	public static final int BOARD = 4;
	public static final int PROJECTOR = 5;
	public static final int CLOSE = 6;
	//ユーザ用
	public static final int EMPLOYEE_ID = 1;
	public static final int LOGIN_ID = 2;
	public static final int PASS = 3;
	public static final int TYPE_ID = 4;

	/**
	 * エンド行2列目に書かれている数値<br>
	 * 定数名：ROW_END_NUM<br>
	 * 値：1
	 */
	public static final int END_NUM = 1;

	public static final String EMPLOYEE = "employeeId";
	public static final String LOGIN = "loginId";
	public static final String COMMA = ",";
	public static final String DATA = "D";
	public static final String START = "S";
}
