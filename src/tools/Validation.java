package tools;

import java.text.ParseException;
//
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

public class Validation {

	private Validation() {
	}

	/**
	 * 空文字、またはnullかどうかを判定するメソッド
	 * @param str
	 * @return boolean
	 */
	public static boolean checkNull(String str) {
		boolean ret = true;
		if (str == null || str.isEmpty()) {
			ret = false;
		}
		return ret;
	}

	/**
	 * 数値であるか(空文字も可)を判定するメソッド
	 * @param data
	 * @return boolean
	 */
	public static boolean chNum(String data) {
		boolean ret = false;
		if ((data.isEmpty()) || (Pattern.matches("^*[0-9]+$", data))) {
			ret = true;
		}
		return ret;
	}

	/**
	 * 1桁以上の半角数字であるか
	 * @param data
	 * @return boolean
	 */
	public static boolean chNumDig(String data) {
		boolean ret = false;
		if ((data.length() > 0) && (Pattern.matches("^*[0-9]*$", data))) {
			ret = true;
		}
		return ret;
	}

	/**
	 * 1桁の半角数字であるか
	 * @param data
	 * @return boolean
	 */
	public static boolean chNumDig1(String data) {
		boolean ret = false;
		if ((data.length() == 1) && (Pattern.matches("^*[0-9]*$", data))) {
			ret = true;
		}
		return ret;
	}

	/**
	 * 1桁のSHDEのどれかであるか
	 * @param data
	 * @return boolean
	 */
	public static boolean chSHDEDig1(String data) {
		boolean ret = false;
		if ((data.length() == 1) && (Pattern.matches("^*[SHDE]*$", data))) {
			ret = true;
		}
		return ret;
	}

	/**
	 * 10桁以内の半角英数字であるか(-_も可)
	 * @param data
	 * @return boolean
	 */
	public static boolean chStrDig10(String data) {
		boolean ret = false;
		if ((data.length() <= 10) && (Pattern.matches("^[-_0-9a-zA-Z]+$", data))) {
			return true;
		}
		return ret;
	}

	/**
	 * 100桁以内の文字列であるか
	 * @param data
	 * @return boolean
	 */
	public static boolean chStrDig100ja(String data) {
		boolean ret = false;
		if ((data.length() <= 100) && (Pattern.matches("^[0-9a-zA-Z一-龠ぁ-んーァ-ヶー]+$", data))) {
			return true;
		}
		return ret;
	}

	/**
	 * 8~12桁の半角英数字であるか
	 * @param data
	 * @return boolean
	 */
	public static boolean chStrDig812(String data) {
		boolean ret = false;
		if (((data.length() >= 8) && (data.length() <= 12)) && (Pattern.matches("^[0-9a-zA-Z]+$", data))) {
			return true;
		}
		return ret;
	}

	/**
	 * 時刻が正しい形式であるか
	 * @param data
	 * @return boolean
	 */
	public static boolean chClose(String data) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		boolean ret = false;
		sdf.setLenient(false);
		try {
			//Dateをsdfのフォーマットで厳密にチェック
			sdf.parse(data);
			ret = true;

		} catch (ParseException e) {
			//エラーがあるとfalse返却
			ret = false;

		}
		return ret;
	}

}
