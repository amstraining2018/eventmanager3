package tools;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class RecordCheck {
	static String catchER;

	private RecordCheck() {

	}

	/**
	 * チェックを行って間違いがあったらfalseを返却<br>
	 * 全部大丈夫ならばtrueを返却する
	 * @param dir
	 * @return boolean
	 * @throws IOException
	 */
	public static boolean check(String dir) throws IOException {
		boolean ret = false;

		try (FileInputStream fi = new FileInputStream(dir);
				InputStreamReader is = new InputStreamReader(fi, Constants2.SHIFT_JIS);
				BufferedReader br = new BufferedReader(is);) {
			String line;
			//1行ずつデータのある分だけまわす
			while ((line = br.readLine()) != null) {
				//各行をカンマ区切りして配列にする
				String[] data = line.split(Constants2.COMMA, -1);
				String top = data[0];
				//配列の0番目がDの時(データ行)の中身をチェックする
				if (top.equals(Constants2.DATA)) {
					String name = data[Constants2.NAME];
					//nameに中身が存在するかチェック
					ret = Validation.checkNull(name);
					if (!ret) {
						break;
					}

					String capacity = data[Constants2.CAPACITY];
					String mic = data[Constants2.MIC];
					String board = data[Constants2.BOARD];
					String projector = data[Constants2.PROJECTOR];
					String close = data[Constants2.CLOSE];
					//nameが100桁以内の文字列かどうか
					ret = Validation.chStrDig100ja(name);
					if (!ret) {
						break;
					}
					//capacityが空文字かどうか＆1桁以上の半角数字であるか
					ret = Validation.checkNull(capacity);
					if (!ret) {
						break;
					}
					//1桁以上の半角数字であるか
					ret = Validation.chNumDig(capacity);
					if (!ret) {
						break;
					}
					//micが数値であるか(空文字も可)
					ret = Validation.chNum(mic);
					if (!ret) {
						break;
					}
					//boardが数値であるか(空文字も可)
					ret = Validation.chNum(board);
					if (!ret) {
						break;
					}
					//projectorが数値であるか(空文字も可)
					ret = Validation.chNum(projector);
					if (!ret) {
						break;
					}
					//closeが空文字かどうか&時刻が正しい形式であるか
					ret = Validation.checkNull(close);
					if (!ret) {
						break;
					}
					//時刻が正しい形式であるか
					ret = Validation.chClose(close);
					if (!ret) {
						break;
					}
				}
			}
		} catch (IndexOutOfBoundsException ex) {
			catchER = "カンマ区切りが足りない";
			return ret = false;
		}
		return ret;
	}
}
