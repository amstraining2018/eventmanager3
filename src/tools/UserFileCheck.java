package tools;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 *ユーザ一括登録情報のcsvファイルを読み込み、チェックを行うクラス
 * @author saito
 */
public class UserFileCheck {
	static String catchER;

	private UserFileCheck() {

	}

	/**
	 *ユーザ一括登録情報のcsvファイルを読み込み、以下の5つのチェックを行う<br>
	 *1.1行目がSであるかどうか<br>
	 *2.ヘッダ行の項目数が正しいか、2行目がHであるかどうか<br>
	 *3.データ行の項目数がヘッダ行と一致しているか<br>
	 *4.データ行の数とエンド行に書かれている数が一致しているか、最後の行がEであるかどうか<br>
	 *5.1列目がSHDEのいずれか1文字であるかどうか<br>
	 * @param dir
	 * String型。ユーザ一括登録情報のcsvファイル。
	 * @return boolean
	 * @throws IOException
	 * @author saito
	 */
	public static boolean check(String dir) throws IOException {
		boolean ret = false;

		try (FileInputStream in = new FileInputStream(dir);
				InputStreamReader is = new InputStreamReader(in, Constants2.SHIFT_JIS);
				BufferedReader br = new BufferedReader(is);) {

			String line;
			ArrayList<String> filelist = new ArrayList<String>();
			while ((line = br.readLine()) != null) {
				filelist.add(line);
			}

			FileCheckImpl filecheck = new FileCheckImpl();

			// 1個目がSかどうか
			String start = filelist.get(Constants2.START_ROW);
			String[] dataS = start.split(Constants2.COMMA, -1);
			ret = dataS[Constants2.SHDE].equals(Constants2.START);
			if (!ret) {
				return ret;
			}

			//	ヘッダ行　項目の数が正しいかどうか
			//	ヘッダ行が２行目であるかどうか
			String head = filelist.get(Constants2.ITEM_ROW);
			ret = filecheck.userHeadItem(head);
			if (!ret) {
				return ret;
			}

			//	データ行　項目の数が正しいかどうか　（ヘッダ行と比べる
			for (int i = 2; i < filelist.size() - 1; i++) {
				head = filelist.get(Constants2.ITEM_ROW);
				String data = filelist.get(i);
				ret = filecheck.dataItem(head, data);
				if (!ret) {
					return ret;
				}
			}

			//	データの行件数と、Eに記述された数が正しいかどうか
			//	Eが最後の行であるか
			int dataNum = filelist.size() - 3;
			int number = filelist.size();
			String end = filelist.get(number - 1);
			ret = filecheck.dataRowNum(dataNum, end);
			if (!ret) {
				return ret;
			}

			//	SHDE以外がないか
			StringBuffer SHDE = new StringBuffer();
			for (int i = 0; i < filelist.size(); i++) {

				String data = filelist.get(i);
				String[] shde = data.split(Constants2.COMMA, -1);
				String aa = shde[Constants2.START_ROW];
				ret = Validation.chSHDEDig1(aa);
				if (!ret) {
					return ret;
				}
				SHDE.append(shde[Constants2.SHDE]);
			}
			String columnName = SHDE.toString();
			ret = columnName.matches("SHD*E");

		} catch (IndexOutOfBoundsException ex) {
			catchER = "カンマ区切りが足りない";
			return ret = false;
		}
		return ret;
	}

}
