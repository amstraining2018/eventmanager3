package tools;

public interface FileCheckInt {

	//	ファイルが存在するかの確認

	//	ヘッダ行　項目の数が正しいかどうか
	//	ヘッダ行が２行目であるかどうか
	public boolean headItem(String head);

	//ユーザ版　ヘッダ行　項目の数が正しいかどうか
	//	ヘッダ行が２行目であるかどうか
	public boolean userHeadItem(String head);

	//	データ行　項目の数が正しいかどうか　（ヘッダ行と比べる
	public boolean dataItem(String head, String data);

	//	データの行件数と、Eに記述された数が正しいかどうか
	//	Eが最後の行であるか
	public boolean dataRowNum(int data, String end);

}
