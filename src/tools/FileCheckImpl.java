package tools;

/**
 * csvファイルの有効性をチェックするクラス
 * @author saito
 */
public class FileCheckImpl implements FileCheckInt {

	private static final String[] HEAD_ARRAY_1 = { "H", "会議室名", "収容人数", "マイク", "ホワイトボード", "プロジェクター", "施錠時間" };
	private static final String[] HEAD_ARRAY_2 = { "H", "社員ID", "アカウント", "初期パスワード", "権限" };

	/**
	 * 会議室一括登録情報のcsvファイル用<br>
	 * ヘッダ行の情報を項目ごとに配列に格納し、次の2点をチェックする<br>
	 * 1.ヘッダ行と項目数が一致しているか<br>
	 * 2.ヘッダ行を示すHが2行目であるかどうか<br>
	 * @param head
	 * String型。ヘッダ行の情報。
	 * @return boolean
	 * @author saito
	 */
	@Override
	public boolean headItem(String head) {
		boolean ret = false;
		String[] comSep = head.split(Constants2.COMMA, -1);
		int comSeplength = comSep.length;
		String[] headArray = HEAD_ARRAY_1;
		int headlength = headArray.length;
		if (comSeplength == headlength) {
			ret = true;
			for (int i = 0; i < comSep.length; i++) {
				if (!headArray[i].equals(comSep[i])) {
					ret = false;
					break;
				}
			}
		}
		return ret;
	}

	/**
	 * データ行の項目とヘッダ行の項目をそれぞれ配列に格納し、
	 * 各行の項目数が一致しているかをチェックする<br>
	 * @param data
	 * String型。データ行の情報。
	 * @param head
	 * String型。ヘッダ行の情報。
	 * @return boolean
	 * @author saito
	 */
	@Override
	public boolean dataItem(String data, String head) {
		boolean ret = false;
		String[] comSep = head.split(Constants2.COMMA, -1);
		String[] comSep2 = data.split(Constants2.COMMA, -1);
		if (comSep.length == comSep2.length) {
			ret = true;
		}
		return ret;
	}

	/**
	 * 次の2点をチェックする<br>
	 * 1.データ行の件数とエンド行に書かれている数値が一致しているか<br>
	 * 2.エンド行を示すEが最後の行であるかどうか
	 * @param data
	 * int型。データ行の件数。
	 * @param end
	 * String型。エンド行の情報。
	 */
	@Override
	public boolean dataRowNum(int data, String end) throws ArrayIndexOutOfBoundsException, NumberFormatException {
		boolean ret = false;
		String[] comSep = end.split(Constants2.COMMA, -1);
		if (comSep[Constants2.SHDE].equals("E")) {
			int endNum = Integer.parseInt(comSep[Constants2.END_NUM]);
			if (data == endNum) {
				ret = true;
			}
		}
		return ret;
	}

	/**
	 * ユーザ一括登録情報のcsvファイル用<br>
	 * ヘッダ行の情報を項目ごとに配列に格納し、次の2点をチェックする<br>
	 * 1.ヘッダ行と項目数が一致しているか<br>
	 * 2.ヘッダ行を示すHが2行目であるかどうか<br>
	 * @param head
	 * String型。ヘッダ行の情報。
	 * @return boolean
	 * @author saito
	 */
	@Override
	public boolean userHeadItem(String head) {
		boolean ret = false;
		String[] comSep = head.split(Constants2.COMMA, -1);
		int comSeplength = comSep.length;
		String[] headArray = HEAD_ARRAY_2;
		int headlength = headArray.length;

		if (comSeplength == headlength) {
			ret = true;
			for (int i = 0; i < comSep.length; i++) {
				if (!headArray[i].equals(comSep[i])) {
					ret = false;
					break;
				}
			}
		}
		return ret;
	}
}