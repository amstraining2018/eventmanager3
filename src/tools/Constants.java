package tools;

/**
*
* The copyright to the computer program(s) herein is the property of icloud inc.
*  Chapter03.オブジェクト指向プログラミング
*  Section2 クラストオブジェクト
*
* プログラム中にあるコメントに従って定数を定義してください。
*
* @author trainee
* @version 1.0
*
*/

/**
 * 定数クラス
 */
public class Constants {
	// 処理区分

	/**
	 * 入力データ区分：エラー検出番号正常<br>
	 * 定数名：SAFE
	 * 値：0<br>
	 */
	public static final Integer SAFE = 0;

	/**
	 * 入力データ区分：エラー検出番号時間間違い<br>
	 * 定数名：TIME_ERROR
	 * 値：-101<br>
	 */
	public static final Integer TIME_ERROR = -101;

	/**
	 * 入力データ区分：エラー検出番号時間のフォーマットに間違い<br>
	 * 定数名：TIME_FORMAT_ERROR
	 * 値：-3<br>
	 */
	public static final Integer TIME_FORMAT_ERROR = -3;

	/**
	 * 入力データ区分：エラー検出番号<br>
	 * 定数名：TIME_PAST_ERROR
	 * 値：-1<br>
	 */
	public static final Integer TIME_PAST_ERROR = -1;

	/**
	 * 入力データ区分：エラー検出番号<br>
	 * 定数名：START_BEFORE_END_ERROR
	 * 値：-2<br>
	 */
	public static final Integer START_BEFORE_END_ERROR = -2;

	/**
	 * 入力データ区分：エラー検出番号<br>
	 * 定数名：USER_ID_DUPLICATE
	 * 値：-1<br>
	 */
	public static final Integer USER_ID_DUPLICATE = -1;

	/**
	 * 入力データ区分：エラー検出番号<br>
	 * 定数名：NO_EMPLOYEEID_ERROR
	 * 値：-2<br>
	 */
	public static final Integer NO_EMPLOYEEID_ERROR = -2;

	/**
	 * 入力データ区分：エラー検出番号ファイル内容に間違い<br>
	 * 定数名：NO_FILE_ERROR
	 * 値：-3<br>
	 */
	public static final Integer NO_FILE_ERROR = -3;

	/**
	 * 入力データ区分：エラー検出番号データ内容に間違い<br>
	 * 定数名：FILE_CONTENTS_ERROR
	 * 値：-4<br>
	 */
	public static final Integer FILE_CONTENTS_ERROR = -4;

	/**
	 * 入力データ区分：エラー検出番号<br>
	 * 定数名：RECORD_CONTENTS_ERROR
	 * 値：-5<br>
	 */
	public static final Integer RECORD_CONTENTS_ERROR = -5;

	/**
	 * 入力データ区分：エラー検出番号ファイル内でテータ重複<br>
	 * 定数名：RECORD_CONTENTS_DUPLICATE
	 * 値：-6<br>
	 */
	public static final Integer RECORD_CONTENTS_DUPLICATE = -6;

	/**
	 * 入力データ区分：エラー検出番号既存データとの重複<br>
	 * 定数名：RECORD_DUPLICATE
	 * 値：-7<br>
	 */
	public static final Integer RECORD_DUPLICATE = -7;

	/**
	 * 入力データ区分：エラー検出番号IOException<br>
	 * 定数名：FILE_OPEN_ERROR
	 * 値：-8<br>
	 */
	public static final Integer FILE_OPEN_ERROR = -8;

	/**
	 * 入力データ区分：エラー検出番号SQLException<br>
	 * 定数名：INSERT_ERROR
	 * 値：-9<br>
	 */
	public static final Integer INSERT_ERROR = -9;

	/**
	 * 入力データ区分：startかendの分岐を行うための番号<br>
	 * 定数名：START
	 * 値：1<br>
	 */
	public static final int START = 1;

	/**
	 * 入力データ区分：startかendの分岐を行うための番号<br>
	 * 定数名：END
	 * 値：2<br>
	 */
	public static final int END = 2;
}
