package tools;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DuplicateCheck {
	static String catchER;

	private DuplicateCheck() {

	}

	/**
	 * 読み込んだデータ内で重複がないかチェックするメソッド
	 * @return boolean
	 * 重複アリfalse 重複ナシtrue
	 * @author saito
	 */
	public static boolean check(String dir, int i) throws IOException {
		boolean rs = true;
		String line;
		List<String> duplicatebox = new ArrayList<>();
		//					ファイル読み込み
		try (FileInputStream fi = new FileInputStream(dir);
				InputStreamReader is = new InputStreamReader(fi, Constants2.SHIFT_JIS);
				BufferedReader br = new BufferedReader(is);) {
			//			一行ずつ読み込み
			while ((line = br.readLine()) != null) {
				String[] data = line.split(Constants2.COMMA, -1);
				String top = data[Constants2.SHDE];
				String box = data[i];
				//				D行の時会議室名もしくは社員ID、ログインIDをlistに格納
				if (top.equals(Constants2.DATA)) {
					duplicatebox.add(box);
				}
			}
			//			iに配列以外の数字が格納された場合catchされる
		} catch (IndexOutOfBoundsException ex) {
			catchER = "範囲外の数字が変数に代入";
			return rs = false;
		}

		Set<String> checkHash = new HashSet<String>();//重複を許さない配列
		for (String box : duplicatebox) {
			if (checkHash.contains(box)) {
				// 重複があればfalseをセットし終了
				rs = false;
				break;
			} else {
				checkHash.add(box);
			}
		}

		return rs;

	}
}
