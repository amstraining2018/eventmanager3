package tools;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserRecordCheck {
	static String catchER;

	public UserRecordCheck() {

	}

	/**
	 * チェックを行って間違いがあったらfalseを返却<br>
	 * 全部大丈夫ならばtrueを返却する
	 * @param dir
	 * @return boolean
	 * @throws IOException
	 */
	public static boolean check(String dir) throws IOException {
		boolean ret = false;

		try (FileInputStream in = new FileInputStream(dir);
				InputStreamReader is = new InputStreamReader(in, Constants2.SHIFT_JIS);
				BufferedReader br = new BufferedReader(is);) {
			String line;
			//1行ずつデータのある分だけまわす
			while ((line = br.readLine()) != null) {
				//各行をカンマ区切りして配列にする
				String[] data = line.split(Constants2.COMMA, -1);
				String top = data[Constants2.START_ROW];
				//配列の0番目がDの時(データ行)の中身をチェックする
				if (top.equals(Constants2.DATA)) {
					String employee_id = data[Constants2.EMPLOYEE_ID];
					//employee_idに中身が存在するかチェック
					ret = Validation.checkNull(employee_id);
					if (!ret) {
						break;
					}

					//employee_idが10桁以内の半角英数かどうか
					ret = Validation.chStrDig10(employee_id);
					if (!ret) {
						break;
					}

					String login_id = data[Constants2.LOGIN_ID];
					String pass = data[Constants2.PASS];
					String type_id = data[Constants2.TYPE_ID];
					//   login_idに中身が存在するかチェック
					ret = Validation.checkNull(login_id);
					if (!ret) {
						break;
					}

					//login_idが10桁以内の文字列かどうか
					ret = Validation.chStrDig10(login_id);
					if (!ret) {
						break;
					}

					//passに中身が存在するかチェック
					ret = Validation.checkNull(pass);
					if (!ret) {
						break;
					}

					//passが8~12桁の文字列かどうか
					ret = Validation.chStrDig812(pass);
					if (!ret) {
						break;
					}

					//type_idに中身が存在するかチェック
					ret = Validation.checkNull(type_id);
					if (!ret) {
						break;
					}

					//type_idが1桁の数字であることの確認
					ret = Validation.chNumDig1(type_id);
					if (!ret) {
						break;
					}

				}
			}
		} catch (IndexOutOfBoundsException ex) {
			catchER = "カンマ区切りが足りない";
			return ret = false;
		}

		return ret;
	}
}
