package tools;

import static tools.Constants.*;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeEr {

	private TimeEr() {

	}

	/**
	 * 時間のフォーマットが不正でないかをチェックするメソッド
	 * @param start
	 * @return Integer
	 */
	public static Integer checkTimeFmt(String timefmt) {
		Integer err;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		//厳密に日付をチェックする
		sdf.setLenient(false);
		try {
			//Dateをsdfのフォーマットで厳密にチェック
			sdf.parse(timefmt);
			err = SAFE;

		} catch (Exception e) {
			err = TIME_ERROR;
			return err;
		}
		return err;
	}

	/**
	 * 過去の時間または終了時間が開始時間より前にならないための比較メソッド<br>
	 * date2がdate1より過去である場合TIME_ERRORが返却<br>
	 * daate2がdate1より未来である場合SAFEが返却<br>
	 * @param date1
	 * @param date2
	 * @return Integer
	 */
	public static Integer checkPastTime(Date date1, Date date2) {

		Integer err = SAFE;
		if (date2.before(date1)) {
			err = TIME_ERROR;
		}
		return err;
	}

}
