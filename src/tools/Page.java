package tools;

public class Page {
	final static int ROW_NUMBER_PAGE = 5;

	private Page() {

	}

	/**
	 * 全ページ数を表す（1ページ5件で計算）
	 * @param tableRowNumber
	 * int型。該当するテーブルの行数。
	 * @return int
	 */
	public static int allPageNumber(int tableRowNumber) {
		int allPageNumber = 0;

		allPageNumber = (tableRowNumber - 1) / ROW_NUMBER_PAGE + 1;

		return allPageNumber;
	}

}
