package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * 説明	このプログラムはサーブレットから呼び出してください
 *
 * サーブレットに下記を追記することでサーブレットから本クラスを呼び出すことができます
 *
 *  ServletContext context = this.getServletContext();
 * 	String path = context.getRealPath(""/WEB-INF/classes/message_ja.properties"");
 * 	OutPutPropertiesGetter p = new OutPutPropertiesGetter();
 * 	p.getProperties(path);
 *
 *
 * */
public class OutPutPropertiesGetter {

	/**
	 * プロパティファイルを読み込み、"file.outPutPath"の値を返すメソッド
	 * @param path
	 * プロパティファイルのパス
	 * @return
	 * String型。"file.outPutPath"の値
	 * @throws IOException
	 */
	public String getProperties(String path) throws IOException {
		String dir = null;
		Path path1 = Paths.get(path);

		try (BufferedReader reader = Files.newBufferedReader(path1, StandardCharsets.UTF_8)) {
			Properties pr = new Properties();
			pr.load(reader);
			String adress = pr.getProperty("file.outPutPath");
			dir = adress;

		} catch (IOException ex) {
			throw new IOException("Not Found File Error");
		}
		return dir;
	}

}
