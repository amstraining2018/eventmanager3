package util;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import domain.EventForFileOutPut;

public class FileOutPut {

	//確実に出力する文字列を設定
	private static final String GROUP = "対象部署,";
	private static final String ROOM = "会議室,";
	private static final String COMMA = ",,,,";
	private static final String HEADER = "タイトル,年月日,開始時間,会議室,対象部署,参加者数";

	/**
	 * イベント一覧の情報をCSVファイルに出力するメソッド
	 * @author R_Kotaki
	 *
	 * @param specifiedGroupName
	 * CSVファイルの1行目：対象部署の情報
	 * @param specifiedConferenceRoom
	 * CSVファイルの2行目：対象会議室の情報
	 * @param EventList
	 * CSVファイルの3行目以降：イベントの情報
	 * @param path
	 * CSVファイルを出力する場所
	 */

	public void fileOutPut(String specifiedGroupName,
			String specifiedConferenceRoom,
			List<EventForFileOutPut> EventList,
			String path) {

		// FileWriterオブジェクト用変数
		FileWriter fw = null;

		// BufferedWriterオブジェクト用変数
		BufferedWriter bw = null;

		try {
			//CSVファイル名の日付表示のためのフォーマット
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date dt1 = new Date();
			// FileWriterオブジェクト作成
			fw = new FileWriter(path + "/events_" + sdf.format(dt1) + ".csv");
			// BufferedWriterオブジェクト作成
			bw = new BufferedWriter(fw);

			//必ず書く文字列をCSVファイルに出力
			bw.write(GROUP);
			bw.write(specifiedGroupName);
			bw.write(COMMA);
			bw.newLine();

			bw.write(ROOM);
			bw.write(specifiedConferenceRoom);
			bw.write(COMMA);
			bw.newLine();

			bw.write(HEADER);
			bw.newLine();

			//年月日,開始時間の日付表示フォーマット
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd,HH:mm");

			//EventListの要素数だけ繰り返し、イベントを書き込み
			for (int i = 0; i < EventList.size(); i++) {

				bw.write(EventList.get(i).getTitle() + "," + sdf2.format(EventList.get(i).getStart()) + ","
						+ EventList.get(i).getPlaceName() + ","
						+ EventList.get(i).getGroupName() + "," + EventList.get(i).getNumberOfAttendingUser());
				bw.newLine();
			}
			// バッファ内の値を出力
			bw.flush();

		} catch (FileNotFoundException e) {
			// ファイル使用中例外発生の場合
			System.out.println(e.getMessage());
			// スタックトレースをコンソールに表示
			e.printStackTrace();
		} catch (IOException e) {
			// その他の出力例外発生の場合
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			// 必ずクローズは行う
			try {
				// ファイルオープン済み判定
				if (bw != null) {
					// ファイルクローズ
					bw.close();
				}
			} catch (IOException e) {
				// クローズで入出力例外発生の場合
				System.out.println(e.getMessage());
			}
		}

	}

}
