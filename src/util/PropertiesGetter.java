package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * 説明	このプログラムはサーブレットから呼び出してください<br>
 *<br>
 * サーブレットに下記を追記することでサーブレットから本クラスを呼び出すことができます<br>
 *<br>
 *  ServletContext context = this.getServletContext();<br>
 * 	String path = context.getRealPath(""/WEB-INF/classes/message_ja.properties"");<br>
 * 	PropertiesGetter p = new PropertiesGetter();<br>
 * 	p.getProperties(path);
 *
 *
 *
 *
 *
 * */
public class PropertiesGetter {

	public static final String USER = "user";
	public static final String ROOM = "room";

	/**
	 * プロパティファイルを読み込み、指定したキーの値を返すメソッド。<br>
	 * ファイルが読み込めなかった場合"err1"を、キーが存在しなかった場合"err0"を返す<br>
	 * @param inputType
	 * プロパティファイルのキー
	 * @param path
	 * プロパティファイルのパス
	 * @return String型
	 */
	public String getProperties(String inputType, String path) {
		String dir = null;
		Path path1 = Paths.get(path);
		try (BufferedReader reader = Files.newBufferedReader(path1, StandardCharsets.UTF_8)) {
			Properties pr = new Properties();
			pr.load(reader);
			if (inputType.equals(ROOM)) {
				String adress = pr.getProperty("file.input");
				dir = adress;
			} else if (inputType.equals(USER)) {
				String adress = pr.getProperty("file.inputUser");
				dir = adress;
			} else {
				dir = "err0";
			}

		} catch (IOException ex) {
			dir = "err1";
		}
		return dir;
	}

}
