package dao;

import java.util.List;

import domain.EventForFileOutPut;

public interface FileOutPutDao {

	/**
	 * グループIDを参照して所属の名前を取得するメソッド
	 * @param groupID
	 * 引数はint型のgroupID
	 * @return
	 * 返り値はString型
	 * @author S_Hishida
	 */
	public String getGroupNmaeByGroupID(int groupID) throws Exception;

	/**
	 * ルームIDを参照して会議室の名前を取得するメソッド
	 * @param roomID
	 * 引数はint型のroomID
	 * @return
	 * 返り値はString型
	 * @author S_Hishida
	 */
	public String getRoomNameByRoomID(int roomID) throws Exception;

	/**
	 * グループIDとルームIDを参照してイベントリストを取得するメソッド
	 * @param groupID,roomID
	 * 引数はint型のgroupIDとint型のroomID
	 * @return
	 * 返り値はList<EventForFileOutPut>型
	 * @author S_Hishida
	 */
	public List<EventForFileOutPut> getEventListByGroupNameRoomName(int groupID,
			int roomID) throws Exception;

}
