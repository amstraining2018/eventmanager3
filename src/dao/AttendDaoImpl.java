package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import domain.Attend;

public class AttendDaoImpl implements AttendDao {
	private DataSource ds;

	public AttendDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	/**
	 * イベントの参加ボタン押下後に追加するためのメソッド
	 * @param attend
	 */
	@Override
	public void insert(Attend attend) throws Exception {
		//attendsテーブルにどのユーザがどのイベントに参加するかを登録する
		try (Connection con = ds.getConnection()) {
			String sql = "insert into attends "
					+ "(attends.user_id,attends.event_id) "
					+ "values(?,?)";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, attend.getAttendUserId(), Types.INTEGER);
			stmt.setObject(2, attend.getAttendEventId(), Types.INTEGER);
			//すべて更新
			stmt.executeUpdate();
		}
	}

	/**
	 * ログインしたユーザが参加するイベントをリスト化するメソッド
	 * @param loginId
	 * @return List
	 */
	@Override
	public List<Attend> findByLoginId(String loginId) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		List<Attend> attendList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT  " +
					"attends.event_id " +
					"FROM " +
					"users " +
					"JOIN " +
					"attends ON users.id = attends.user_id " +
					"WHERE " +
					"users.login_id = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			//？にログインしているユーザのloginIdを入れる
			stmt.setString(1, loginId);
			ResultSet rs = stmt.executeQuery();
			//次のデータがなくなったらfalse
			while (rs.next()) {
				//データを1つずつ追加
				attendList.add(mapToAttendA(rs));
			}
		}
		return attendList;
	}

	/**
	 * ログインしたユーザが参加するイベント情報をセットするメソッド
	 * @param rs
	 * @return Attend
	 * @throws SQLException
	 */
	private Attend mapToAttendA(ResultSet rs) throws SQLException {
		Attend attend = new Attend();
		//event_idをInteger型でセット
		attend.setAttendEventId((Integer) rs.getObject("event_id"));
		return attend;
	}

	/**
	 * ユーザがイベントに参加しているか否かの判断をするためのメソッド
	 * @param attend
	 * @return boolean
	 */
	@Override
	public boolean joining(Attend attend) throws Exception {
		boolean joining;
		try (Connection con = ds.getConnection()) {
			String sql = "select * from attends  "
					+ "where attends.user_id=? "
					+ "And attends.event_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, attend.getAttendUserId(), Types.INTEGER);
			stmt.setObject(2, attend.getAttendEventId(), Types.INTEGER);
			ResultSet rs = stmt.executeQuery();
			//参加しているもの(SQL文中で一致したもの)があればif文に入る
			if (rs.next()) {
				joining = true;
			} else {
				joining = false;
			}
		}
		return joining;
	}

	/**
	 * attendsテーブルからユーザの参加を取り消すメソッド(参加取り消しボタン)
	 * @param attend
	 */
	@Override
	public void attendDelete(Attend attend) throws Exception {
		try (Connection con = ds.getConnection()) {
			//user_idとevent_idが一致したときにその行をattendsテーブルから削除
			String sql = "delete from attends  "
					+ "where attends.user_id=? "
					+ "And attends.event_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, attend.getAttendUserId(), Types.INTEGER);
			stmt.setObject(2, attend.getAttendEventId(), Types.INTEGER);
			stmt.executeUpdate();
		}
	}

	/**
	 * イベントが削除された際にattendsテーブルからevent_idを削除するメソッド
	 * @param attend
	 */
	@Override
	public void attendDeleteEventId(Attend attend) throws Exception {
		try (Connection con = ds.getConnection()) {
			String sql = "delete from attends  "
					+ "where attends.event_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			//eventIdを?にセット
			stmt.setObject(1, attend.getAttendEventId(), Types.INTEGER);
			stmt.executeUpdate();
		}
	}

	/**
	 * ユーザが削除された際にattendsテーブルからuser_idを削除するメソッド
	 */
	@Override
	public void attendDeleteUserId(Attend attend) throws Exception {
		try (Connection con = ds.getConnection()) {
			String sql = "delete from attends  "
					+ "where attends.user_id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			//user_idを?にセット
			stmt.setObject(1, attend.getAttendUserId(), Types.INTEGER);
			stmt.executeUpdate();
		}
	}
}
