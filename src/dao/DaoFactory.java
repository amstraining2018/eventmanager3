package dao;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 *  DaoFactoryクラスです
 *  DaoFactoryクラス内では、DAOクラスのインスタンスを生成する際にDataSourceを取得し
 *  Daoクラスのコンストラクタへ渡します
 *
 *
 * @author trainee
 * @version 1.0
 */
public class DaoFactory {
	public static UserDao createUserDao() {
		return new UserDaoImpl(getDataSource());
	}

	public static LoginDao createLoginDao() {
		return new LoginDaoImpl(getDataSource());
	}

	public static EventDao createEventDao() {
		return new EventDaoImpl(getDataSource());
	}

	public static AttendDao createAttendDao() {
		return new AttendDaoImpl(getDataSource());
	}

	public static RoomDao createRoomDao() {
		return new RoomDaoImpl(getDataSource());
	}

	public static FileOutPutDao createFileOutPutDao() {
		return new FileOutPutDaoImpl(getDataSource());
	}

	public static UserTypesDao createUser_TypesDao() {
		return new UserTypesDaoImpl(getDataSource());
	}

	public static GroupListDao createGroupListDao() {
		return new GroupListDaoImpl(getDataSource());
	}

	public static EmployeeDao createEmployeeDao() {
		return new EmployeeDaoImpl(getDataSource());
	}

	/**
	 *
	 * @author kotaki
	 */
	public static EventSearchDao createEventSearchDao() {
		return new EventSearchDaoImpl(getDataSource());
	}

	private static DataSource getDataSource() {

		InitialContext ctx = null;
		DataSource ds = null;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/kawamuradb");
		} catch (NamingException e) {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException el) {
					throw new RuntimeException(el);
				}

			}
			throw new RuntimeException(e);
		}
		return ds;

	}

}
