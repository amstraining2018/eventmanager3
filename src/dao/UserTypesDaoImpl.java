package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

public class UserTypesDaoImpl implements UserTypesDao {

	private DataSource ds;

	public UserTypesDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	/**
	 *ユーザ一括登録の際に入力されたtype_idが、
	 *user_Typesテーブルに存在する数字でなければ登録不可となりfalseを返却
	 * @param data
	 * Integer型。ユーザ一括登録の際に入力されたtype_id。
	 * @return boolean
	 */
	@Override
	public boolean checkExsist(Integer data) throws SQLException {
		boolean ret = false;
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT COUNT(*) FROM user_Types WHERE id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, data);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			int count = rs.getInt(1);
			if (count == 1) {
				ret = true;
			} else {
				ret = false;
			}
		}
		return ret;

	}

}
