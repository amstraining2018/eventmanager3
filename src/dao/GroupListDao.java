package dao;

import java.util.List;

import domain.Group;

public interface GroupListDao {

	public List<Group> findAll() throws Exception;
}
