package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import domain.Room;

//後で消す
public class RoomDaoImpl implements RoomDao {

	private DataSource ds;

	public RoomDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	/**
	 * 会議室を登録するメソッド
	 * @param room
	 */
	@Override
	public void insert(Room room) throws SQLException {

		try (Connection con = ds.getConnection()) {
			String sql = "INSERT INTO rooms"
					+ "(name,capacity,mic,board,projector,close)"
					+ "VALUES(?,?,?,?,?,?)";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, room.getName());
			stmt.setObject(2, room.getCapacity(), Types.INTEGER);
			stmt.setObject(3, room.getMic(), Types.INTEGER);
			stmt.setObject(4, room.getBoard(), Types.INTEGER);
			stmt.setObject(5, room.getProjector(), Types.INTEGER);
			stmt.setString(6, room.getClose());
			stmt.executeUpdate();
		}
	}

	/**
	 * 会議室に同じ名前のものがないかをチェックするメソッド
	 * @param data
	 * @return boolean
	 */
	@Override
	public boolean checkExsist(String data) throws SQLException {
		boolean ret = false;
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT COUNT(*) FROM rooms WHERE name=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, data);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			int count = rs.getInt(1);
			if (count == 0) {
				ret = true;
			} else {
				ret = false;
			}
		}
		return ret;
	}

	/**
	 * イベント登録等のページの開催場所項目をセレクト式にする際に、必要な情報(会議室番号と名前)を取得するメソッド。
	 * @return List<Room>
	 * 開催場所の情報(部屋名とID)が入ったRoomリスト
	 * @author kotaki
	 */
	@Override
	public List<Room> findAll() throws SQLException {
		List<Room> roomList = new ArrayList<>();
		try (Connection conn = ds.getConnection()) {
			String sql = "Select id,name from rooms";
			PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Room room = new Room();
				room.setId(rs.getInt("id"));
				room.setName(rs.getString("name"));
				roomList.add(room);
			}
		}
		return roomList;

	}
}
