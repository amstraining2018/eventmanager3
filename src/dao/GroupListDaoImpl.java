package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import domain.Group;

public class GroupListDaoImpl implements GroupListDao {
	private DataSource ds;

	public GroupListDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	/**
	 * イベント登録等のページの対象部署項目をセレクト式にする際に、必要な情報(会議室番号と名前)を取得するメソッド。
	 * @return List<Group>
	 * 所属の情報(部署名とID)が入ったGroupリスト
	 * @author kotaki
	 */
	@Override
	public List<Group> findAll() throws Exception {
		List<Group> groupList = new ArrayList<>();
		try (Connection conn = ds.getConnection()) {
			String sql = "SELECT * FROM groups";
			PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Group group = new Group();
				group.setId(rs.getInt("id"));
				group.setName(rs.getString("name"));
				groupList.add(group);
			}
		}
		return groupList;
	}
}
