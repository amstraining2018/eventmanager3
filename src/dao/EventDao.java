package dao;

import java.util.List;

import domain.Attend;
import domain.Event;

public interface EventDao {
	//	イベントの一覧を表示させる
	public List<Event> findAll(Integer PageCount, List<Attend> attendEvent) throws Exception;

	//イベントの詳細を表示させる
	public Event findByEventId(Event event) throws Exception;

	//	本日のイベントを表示させる
	public List<Event> findByToday(Integer PageCount, List<Attend> attendEvent) throws Exception;

	//	イベントを登録する
	public void insert(Event event) throws Exception;

	//	イベントを編集する
	public void update(Event event) throws Exception;

	//	イベントを削除する
	public void delete(Event event) throws Exception;

	//ユーザが登録したイベントの情報をeventsテーブルから削除する
	public void deleteFindUserId(Attend attend) throws Exception;

	//イベントの参加者を表示させる
	public List<Event> findAttendName(Event event) throws Exception;

	//イベントの行数を取得する
	public Integer countRow() throws Exception;

	//本日のイベントの行数を取得する
	public Integer countToday() throws Exception;

}
