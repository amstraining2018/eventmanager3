package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.mindrot.jbcrypt.BCrypt;

import domain.Login;

public class LoginDaoImpl implements LoginDao {

	private DataSource ds;

	public LoginDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	/**
	 * ログインIDとログインパスワードを取得するメソッド
	 * @param loginId
	 * String型 usersテーブルのlogin_id
	 * @param loginPass
	 * String型 usersテーブルのlogin_pass
	 * @return login
	 * Login型
	 */

	@Override
	public Login findByLoginIdAndLoginPass(String loginId, String loginPass) throws Exception {
		//初期値を代入
		Login login = null;

		try (Connection con = ds.getConnection()) {
			String sql = "SELECT  " +
					"    users.id as id, " +
					"    users.login_id as login_id, " +
					"    users.login_pass as login_pass, " +
					"    employee.name as name, " +
					"    users.type_id as type_id, " +
					"    employee.group_id as group_id " +
					"FROM " +
					"    users " +
					"        JOIN " +
					"    employee ON users.employee_id = employee.Employee_id " +
					"WHERE " +
					"    login_id = ?";

			//SQL文を実行するための準備
			PreparedStatement stmt = con.prepareStatement(sql);

			//？に代入する値をセット
			stmt.setString(1, loginId);

			//SQL文の実行
			ResultSet rs = stmt.executeQuery();

			//rsを一行読み込み
			if (rs.next()) {
				//ハッシュ化したパスワードを比べる
				if (BCrypt.checkpw(loginPass, rs.getString("login_pass"))) {
					login = mapToEvent(rs);
				}
			}
		}
		return login;

	}

	private Login mapToEvent(ResultSet rs) throws SQLException {

		//loginインスタンスを作成
		Login login = new Login();

		//インスタンスに値をセット
		login.setUserId((Integer) rs.getObject("id"));
		login.setLoginId(rs.getString("login_id"));
		login.setUserName(rs.getString("name"));
		login.setTypeId((Integer) rs.getObject("type_id"));
		login.setGroupId((Integer) rs.getObject("group_id"));

		return login;
	}

}
