package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import domain.Attend;
import domain.Event;

public class EventDaoImpl implements EventDao {

	private DataSource ds;

	public EventDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	/**
	 * そのページで先頭になる行が全体で何行目かを取り出す式
	 * @param PageCount, attendEvent
	 * @return List
	 */
	@Override
	public List<Event> findAll(Integer PageCount, List<Attend> attendEvent) throws Exception {
		//Limit句は0から数え始めるため初期値は0
		Integer p = 0;
		if (PageCount != null) {
			//1ページ目は(0から数えて)1行目スタート、2ページ目は(0から数えて)6行目スタート…
			p = p + (PageCount - 1) * 5;
		}
		List<Event> eventList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT " +
					"events.id, " +
					"events.title, " +
					"events.start, " +
					"rooms.name AS place, " +
					"groups.name AS groupName " +
					"FROM " +
					"events " +
					"JOIN " +
					"groups ON events.group_id = groups.id " +
					"JOIN " +
					"rooms ON events.room_id = rooms.id " +
					"ORDER BY events.start DESC " + //開始日時を降順に並べ替える
					"limit ?,5";//？に入力された数字から5つ分のデータを取り出す(0からカウントする)
			PreparedStatement stmt = con.prepareStatement(sql);
			//？にはそのページで先頭になる行の全体での番号pが入る(Integer型に指定)
			stmt.setObject(1, p, Types.INTEGER);
			//表にあるデータの中で、カーソルが現在の行を指す(初期状態では先頭を指す)
			ResultSet rs = stmt.executeQuery();
			//次にデータがなくなるとfalseを返してループを抜ける
			while (rs.next()) {
				//eventListに追加するのはrsとattendEvent(1つずつデータを追加している)
				eventList.add(mapToEventFindAll(rs, attendEvent));
			}
		}
		//eventListのデータ(イベントの一覧)を持って帰る
		return eventList;
	}

	/**
	 * イベントIDでイベントの詳細を表示させるメソッド
	 * @param event
	 * イベントIDがセットされてるイベントをセットする
	 * @return Event
	 * イベント詳細用の情報が入っているイベントインスタンス
	 * @author kotaki
	 */
	@Override
	public Event findByEventId(Event event) throws Exception {
		Event eventDe = new Event();
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT " +
					"events.id, " +
					"events.title, " +
					"events.start, " +
					"events.end, " +
					"rooms.name AS place, " +
					"events.group_id, " +
					"groups.name AS groupName, " +
					"events.detail, " +
					"registered_by, " +
					"employee.name AS registeredByName " +
					"FROM " +
					"events " +
					"JOIN " +
					"groups ON events.group_id = groups.id " +
					"JOIN " +
					"users ON events.registered_by = users.id " +
					"JOIN " +
					"rooms ON events.room_id = rooms.id " +
					"JOIN " +
					"employee ON users.employee_id = employee.Employee_id " +
					"WHERE " +
					"events.id = ?";
			//sql文を実行するための準備
			PreparedStatement stmt = con.prepareStatement(sql);
			//?にeventIdを入れる
			stmt.setObject(1, event.getEventId(), Types.INTEGER);
			//sql文実行
			ResultSet rs = stmt.executeQuery();
			//rsを一行読み込み
			rs.next();
			eventDe = mapToEventFindByEventId(rs);
		}
		return eventDe;
	}

	/**
	 * イベント詳細内容をセットするメソッド
	 */
	private Event mapToEventFindByEventId(ResultSet rs) throws SQLException {
		//eventインスタンス作成
		Event event = new Event();
		event.setEventId((Integer) rs.getObject("id"));//eventsTableのidをeventIdにセット
		event.setTitle(rs.getString("title"));//eventsTableのtitleをtitleにセット
		event.setStart((Date) rs.getObject("start"));//evensTableのstartをstartにセット
		event.setEnd((Date) rs.getObject("end"));//eventsTableのendをendにセット
		event.setPlace(rs.getString("place"));//eventsTableのplaceをplaceにセット
		event.setDetail(rs.getString("detail"));//eventsTableのdetailをdetailにセット
		event.setRegisteredById((Integer) rs.getObject("registered_by"));//eventsTableのregistered_byをregistered_byにセット
		event.setRegisteredByName(rs.getString("registeredByName"));//usersTableのregisteredByNameをregisteredByNameにセット
		event.setGroupId((Integer) rs.getObject("group_id"));//eventTableのgroup_idをgroupIdにセット
		event.setGroupName(rs.getString("groupName"));//groupsTableのgroupNameをgroupNameにセット
		//セットしたeventインスタンスを返す
		return event;
	}

	/**
	 * 本日のイベントをリストで取得する
	 * @param PageCount, attendEvent
	 * @return List
	 */
	@Override
	public List<Event> findByToday(Integer PageCount, List<Attend> attendEvent) throws Exception {
		//SQL文のlimitの第一引数に設定するための変数(初期値を0に設定)
		Integer p = 0;
		//PageCount()に値が入っていればif文に入る
		if (PageCount != null) {
			//各ページの始まりの行数を取得するための式
			p = (PageCount - 1) * 5;
		}
		List<Event> eventList = new ArrayList<>();
		//日時の書式を指定
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd 00:00:00");//日時の書式を設定
		//今日の時刻を取得
		Calendar now = Calendar.getInstance();
		String today = fmt.format(now.getTime());
		//明日の時刻を取得(今日の日付+1)
		now.add(Calendar.DATE, 1);
		String tomorrow = fmt.format(now.getTime());

		try (Connection con = ds.getConnection()) {
			String sql = "SELECT " +
					"events.id, " +
					"events.title, " +
					"events.start, " +
					"rooms.name AS place, " +
					"groups.name AS groupName " +
					"FROM " +
					"events " +
					"JOIN " +
					"groups ON events.group_id = groups.id " +
					"JOIN " +
					"rooms ON events.room_id = rooms.id " +
					"where events.start >= DATE_FORMAT(?,'%Y-%m-%d %H:%i:%s') " + //今日のイベントから
					"And events.start < DATE_FORMAT(?,'%Y-%m-%d %H:%i:%s') " + //次の日のイベントになるまでで
					"ORDER BY events.start DESC " +
					"limit ?,5";//1ページ内に5件ずつを表示させる
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, today);
			stmt.setString(2, tomorrow);
			stmt.setObject(3, p, Types.INTEGER);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				//executeQuery()で取得したテーブル全体のデータの中からmapToEventTで選んだ必要な情報だけをリストに追加していく
				eventList.add(mapToEventToday(rs, attendEvent));
			}
		}
		return eventList;
	}

	/**
	 * 本日のイベント一覧における参加マークの表示メソッド
	 */
	private Event mapToEventToday(ResultSet rs, List<Attend> attendEvent) throws SQLException {
		Event event = new Event();
		int id = rs.getInt("id");
		for (Attend s : attendEvent) {
			//attendsテーブルのevent_idと一致するeventテーブルのidであればif文に入る
			if (s.getAttendEventId() == id) {
				event.setAttendtf(true);
			}
		}
		//本日のイベント一覧に必要な情報をそれぞれにセットする
		event.setEventId((Integer) rs.getObject("id"));
		event.setTitle(rs.getString("title"));
		event.setStart((Date) rs.getObject("start"));
		event.setPlace(rs.getString("place"));
		event.setGroupName(rs.getString("groupName"));
		return event;
	}

	/**
	 * イベント登録用メソッド
	 * @param event
	 * 登録のjspで入力された値が入ってる
	 */
	@Override
	//	イベントを登録するためのメソッド
	public void insert(Event event) throws Exception {
		// 開始と終了の日時を書式指定
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String start = sdFormat.format(event.getStart());
		String end = sdFormat.format(event.getEnd());

		try (Connection con = ds.getConnection()) {
			String sql = "INSERT INTO "
					+ "kawamuradb.events "
					+ "(title, start, end, room_id, group_id, detail, registered_by, created) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, now())";
			PreparedStatement stmt = con.prepareStatement(sql);
			//?に各値をセットする
			stmt.setString(1, event.getTitle());
			stmt.setString(2, start);
			stmt.setString(3, end);
			stmt.setInt(4, event.getRoomId());
			stmt.setObject(5, event.getGroupId());
			stmt.setString(6, event.getDetail());
			stmt.setObject(7, event.getUserId());
			//すべて更新する
			stmt.executeUpdate();
		}
	}

	/**
	 * イベントを編集するためのメソッド
	 * @param event
	 */
	@Override
	public void update(Event event) throws Exception {
		// 開始と終了の日時を書式指定
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String start = sdFormat.format(event.getStart());
		String end = sdFormat.format(event.getEnd());

		try (Connection con = ds.getConnection()) {
			String sql = "update events set "
					+ "title = ?,start=?,end=?,room_id=?,"
					+ "group_id=?,detail=?,registered_by=?,created=now() "
					+ "where id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			//?に各値をセットする
			stmt.setString(1, event.getTitle());
			stmt.setString(2, start);
			stmt.setString(3, end);
			stmt.setInt(4, event.getRoomId());
			stmt.setObject(5, event.getGroupId(), Types.INTEGER);
			stmt.setString(6, event.getDetail());
			stmt.setObject(7, event.getRegisteredById(), Types.INTEGER);
			stmt.setObject(8, event.getEventId(), Types.INTEGER);
			//すべて更新する
			stmt.executeUpdate();
		}
	}

	/**
	 * 	イベントを削除するためのメソッド
	 * @param event
	 */
	@Override
	public void delete(Event event) throws Exception {
		// eventsテーブルのidを取得し、その行を削除する
		try (Connection con = ds.getConnection()) {
			String sql = "delete from events where id=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, event.getEventId(), Types.INTEGER);
			//更新する
			stmt.executeUpdate();
		}
	}

	/**
	 * ユーザが登録したイベントの情報をeventsテーブルから削除するメソッド(ユーザが削除された際に登録者が不在になるため)
	 * @param attend
	 */
	@Override
	public void deleteFindUserId(Attend attend) throws Exception {
		//イベントの登録者をユーザIDから特定し、該当するイベント自体も削除する
		try (Connection con = ds.getConnection()) {
			String sql = "delete from events where registered_by=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, attend.getAttendUserId(), Types.INTEGER);
			//更新する
			stmt.executeUpdate();
		}
	}

	/**
	 * イベント一覧における参加マークの表示メソッド
	 */
	private Event mapToEventFindAll(ResultSet rs, List<Attend> attendEvent) throws SQLException {
		Event event = new Event();
		int id = rs.getInt("id");
		for (Attend s : attendEvent) {
			//取得した(表にあるすべての)データとAttendEventIdで取得したユーザが参加するイベントのデータとが一致しているものだけを取り出す
			if (s.getAttendEventId() == id) {
				//一致しているものをAttendtfにセット(event_idがセットされる)
				event.setAttendtf(true);
			}
		}
		event.setEventId((Integer) rs.getObject("Id"));
		event.setTitle(rs.getString("title"));
		event.setStart((Date) rs.getObject("start"));
		event.setPlace(rs.getString("place"));
		event.setGroupName(rs.getString("groupName"));
		return event;
	}

	/**
	 * イベントの参加者を表示させるメソッド
	 * @param event
	 * @return List
	 */
	@Override
	public List<Event> findAttendName(Event event) throws Exception {
		List<Event> eventList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT  " +
					"employee.name AS attendName " +
					"FROM " +
					"attends " +
					"JOIN " +
					"users ON attends.user_id = users.id " +
					"JOIN " +
					"employee ON users.employee_id = employee.Employee_id " +
					"WHERE " +
					"attends.event_id = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, event.getEventId(), Types.INTEGER);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				eventList.add(mapToEventAttendName(rs));
			}
		}
		return eventList;
	}

	/**
	 * イベントの参加者名を取得するメソッド
	 */
	private Event mapToEventAttendName(ResultSet rs) throws SQLException {
		Event event = new Event();
		event.setAttendName(rs.getString("attendName"));
		return event;
	}

	/**
	 * イベントの行数を取得するためのメソッド
	 * @return Integer
	 */
	@Override
	public Integer countRow() throws Exception {
		Integer rowCount = 0;
		try (Connection con = ds.getConnection()) {
			//eventsテーブルのidの数をカウントして行数を取得する
			String sql = "select Count(id) as rowCount from events ";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				rowCount = rs.getInt("rowCount");
			}
		}
		return rowCount;
	}

	/**
	 * 本日のイベントの行数を取得するためのメソッド
	 * @return Integer
	 */
	@Override
	public Integer countToday() throws Exception {
		Integer rowCount = 0;
		//日時の書式を指定
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
		//今日の日時を取得
		Calendar now = Calendar.getInstance();
		String today = fmt.format(now.getTime());
		//明日の時刻を取得(今日の日付+1)
		now.add(Calendar.DATE, 1);
		String tomorrow = fmt.format(now.getTime());
		//eventsテーブルのidの数をカウントして行数を取得する(今日の日時で条件指定)
		try (Connection con = ds.getConnection()) {
			String sql = "select Count(id) as rowCount from events " +
					"where events.start >= DATE_FORMAT(?,'%Y-%m-%d %H:%i:%s') " +
					"And events.start < DATE_FORMAT(?,'%Y-%m-%d %H:%i:%s') ";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, today);
			stmt.setString(2, tomorrow);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				rowCount = rs.getInt("rowCount");
			}
		}
		return rowCount;
	}
}
