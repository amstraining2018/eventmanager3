package dao;

import java.sql.SQLException;
import java.util.List;

import domain.User;

public interface UserDao {

	public List<User> findAll(Integer PageCount) throws Exception;
	//	ユーザの一覧を表示させます。

	public void insert(User user) throws SQLException;
	//	ユーザ情報を登録します。

	public void update(User user) throws Exception;
	//	ユーザ情報を編集します。

	public void delete(int id) throws Exception;
	//	ユーザ情報を削除します。

	//ユーザの詳細を表示させます。
	public User findByUserId(Integer userId) throws Exception;

	public User userD(Integer user) throws Exception;

	//パスワードのハッシュ化
	public String makePasswordHash(String pass);

	public Integer countRow() throws Exception;

	//パスワードのみの変更
	public void chanegePassword(String pass, Integer id) throws Exception;

	//すでに登録されたデータか検証
	public boolean checkExsist(String column, String data) throws SQLException;

}
