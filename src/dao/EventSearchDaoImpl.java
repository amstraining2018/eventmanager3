package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import domain.Attend;
import domain.Event;

public class EventSearchDaoImpl implements EventSearchDao {

	private DataSource ds;

	public EventSearchDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	@Override
	public List<Event> search(int groupID, int roomID, int currentPageNumber, List<Attend> attendList)
			throws Exception {

		List<Event> eventList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT   " +
					"    events.id, " +
					"    events.title, " +
					"    events.start, " +
					"    rooms.name AS place, " +
					"    groups.name AS groupName " +
					"FROM " +
					"    events " +
					"        JOIN " +
					"    groups ON events.group_id = groups.id " +
					"        JOIN " +
					"    rooms ON events.room_id = rooms.id ";

			String afterSql = "";
			if (groupID != 0 && roomID != 0) {
				afterSql = "WHERE rooms.id = " + roomID + " AND groups.id = " + groupID;
			} else if (roomID != 0) {
				afterSql = "WHERE rooms.id = " + roomID;
			} else if (groupID != 0) {
				afterSql = "WHERE groups.id = " + groupID;
			}

			sql = sql + afterSql + " ORDER BY events.start DESC " + "LIMIT ? , 5";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, (currentPageNumber - 1) * 5, Types.INTEGER);
			ResultSet rs = stmt.executeQuery();

			//次にデータがなくなるとfalseを返してループを抜ける
			while (rs.next()) {
				//eventListに追加するのはrsとattendEvent(1つずつデータを追加している)
				eventList.add(mapToEventForSearch(rs, attendList));
			}
		}

		return eventList;
	}

	private Event mapToEventForSearch(ResultSet rs, List<Attend> attendList) throws SQLException {

		Event event = new Event();

		int id = rs.getInt("id");

		//ログインしているユーザが参加しているイベントであった場合、eventインスタンスのattendtfフィールドをtrueに
		for (Attend attend : attendList) {
			if (attend.getAttendEventId() == id) {
				event.setAttendtf(true);
				break;
			}
		}

		event.setEventId((Integer) rs.getObject("Id"));
		event.setTitle(rs.getString("title"));
		event.setStart((Date) rs.getObject("start"));
		event.setPlace(rs.getString("place"));
		event.setGroupName(rs.getString("groupName"));

		return event;
	}

	@Override
	public int getNumberOfTableRow(int groupID, int roomID) throws Exception {
		int numberOfRow = 0;

		try (Connection con = ds.getConnection()) {
			String sql = "select Count(id) as rowCount from events ";

			String afterSql = "";
			if (groupID != 0 && roomID != 0) {
				afterSql = "WHERE room_id = " + roomID + " AND group_id = " + groupID;
			} else if (roomID != 0) {
				afterSql = "WHERE room_id = " + roomID;
			} else if (groupID != 0) {
				afterSql = "WHERE group_id = " + groupID;
			}
			sql = sql + afterSql;

			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				numberOfRow = rs.getInt("rowCount");
			}
		}
		return numberOfRow;
	}

}
