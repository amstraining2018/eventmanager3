package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import domain.Employee;

public class EmployeeDaoImpl implements EmployeeDao {

	private DataSource ds;

	public EmployeeDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	/**
	 * ユーザ未登録だけどEmployeeテーブルには登録されている人のリスト
	 * @return List<Employee>
	 * ユーザ未登録の人のEmployee_idが格納されている
	 */
	@Override
	public List<Employee> findUnregistered() throws Exception {
		List<Employee> employeeList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT  " +
					"    employee.employee_id " +
					"FROM " +
					"    employee " +
					"        LEFT JOIN " +
					"    users ON (employee.employee_id = users.employee_id) " +
					"WHERE " +
					"    users.employee_id IS NULL";

			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Employee employee = new Employee();
				employee.setEmployee_id(rs.getInt("employee_id"));
				employeeList.add(employee);
			}
		}
		return employeeList;
	}

}
