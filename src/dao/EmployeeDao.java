package dao;

import java.util.List;

import domain.Employee;

public interface EmployeeDao {

	//ユーザ登録されていない人をEmployeeテーブルから見つけてくる
	public List<Employee> findUnregistered() throws Exception;
}
