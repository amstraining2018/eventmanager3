package dao;

import java.sql.SQLException;

public interface UserTypesDao {

	// usertypesに存在する値であるかチェック
	public boolean checkExsist(Integer data) throws SQLException;
}
