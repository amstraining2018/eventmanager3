package dao;

import domain.Login;

public interface LoginDao {

	public Login findByLoginIdAndLoginPass(String loginId, String loginPass) throws Exception;
	//	ログインIDとログインパスワードをSQL内で照合します。

}
