package dao;

import java.sql.SQLException;
import java.util.List;

import domain.Room;

public interface RoomDao {

	public void insert(Room room) throws SQLException;
	//	Roomテーブル情報を登録します

	public boolean checkExsist(String data) throws SQLException;

	public List<Room> findAll() throws SQLException;
}
