package dao;

import java.util.List;

import domain.Attend;
import domain.Event;

/**
 * イベント検索に使用するSQLを実行するためのクラス
 * @author R_kotaki
 */
public interface EventSearchDao {

	/**
	 *指定した所属グループID,会議室IDの、イベント情報が入ったEventインスタンスが入ったリストを返すメソッド。
	 * @param groupID
	 * 所属グループID
	 * @param roomID
	 * 会議室ID
	 * @param currentPageNumber
	 * 表示するページ番号
	 * @param attendList
	 * ログインしているユーザが参加するイベントのリスト
	 * @return
	 * イベント情報が入ったEventインスタンスが入ったリスト
	 * @author R_kotaki
	 */
	public List<Event> search(int groupID, int roomID, int currentPageNumber, List<Attend> attendList) throws Exception;

	/**
	 * 指定した所属グループID,会議室IDのテーブルの行数を取得するメソッド
	 * @param groupID
	 * 所属グループID
	 * @param roomID
	 * 会議室ID
	 * @return
	 * 行数
	 * @author R_kotaki
	 */
	public int getNumberOfTableRow(int groupID, int roomID) throws Exception;
}
