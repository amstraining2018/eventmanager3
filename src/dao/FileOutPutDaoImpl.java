/**
 *
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import domain.EventForFileOutPut;

/**
 * @author user
 *
 */
public class FileOutPutDaoImpl implements FileOutPutDao {

	private DataSource ds;

	public FileOutPutDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	/**
	 * 所属グループIDに対応する所属グループ名を返すメソッド
	 * @param groupID
	 * groupsテーブルのID
	 * @return
	 * IDに対応する所属グループ名
	 */
	@Override
	public String getGroupNmaeByGroupID(int groupID) throws Exception {

		String groupName = null;

		try (Connection con = ds.getConnection()) {
			String sql = "SELECT * FROM kawamuradb.groups where id=?";

			//sql文を実行するための準備
			PreparedStatement stmt = con.prepareStatement(sql);

			//?にgroupIDを入れる
			stmt.setObject(1, groupID, Types.INTEGER);

			//sql文実行
			ResultSet rs = stmt.executeQuery();

			//rsを一行読み込み
			rs.next();
			groupName = rs.getString("name");
		}
		return groupName;
	}

	/**
	 * roomsテーブルのIDに対応する会議室名を返すメソッド
	 * @param roomID
	 * roomsテーブルのID
	 * @return
	 * IDに対応する会議室名
	 */
	@Override
	public String getRoomNameByRoomID(int roomID) throws Exception {

		String roomName = null;
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT name FROM kawamuradb.rooms where id=?;";

			//sql文を実行するための準備
			PreparedStatement stmt = con.prepareStatement(sql);

			//?にroomIDを入れる
			stmt.setObject(1, roomID, Types.INTEGER);

			//sql文実行
			ResultSet rs = stmt.executeQuery();

			//rsを一行読み込み
			rs.next();
			roomName = rs.getString("name");
		}
		return roomName;

	}

	/**
	 * groupID,roomIDを指定して、eventsテーブルから情報を取得するメソッド
	 * @param roomID
	 * roomsテーブルのID。0の場合は指定なし。
	 * @param groupID
	 * groupsテーブルのID。0の場合は指定なし。
	 * @return
	 * 指定したgroupID,roomIDに対応するレコードの情報を持った、Eventインスタンスが入ったリスト。
	 */
	@Override
	public List<EventForFileOutPut> getEventListByGroupNameRoomName(int groupID, int roomID) throws Exception {

		//初期値を設定し、インスタンスの作成して返り値のための変数に代入
		List<EventForFileOutPut> FileOutPutList = new ArrayList<>();

		try (Connection con = ds.getConnection()) {
			String sql = "SELECT  " +
					"    events.title AS title, " +
					"    events.start AS start, " +
					"    rooms.name AS room_name, " +
					"    groups.name AS group_name, " +
					"    attendsCount.count AS count " +
					"FROM " +
					"    (((SELECT  " +
					"        COUNT(user_id) AS count, event_id " +
					"    FROM " +
					"        attends " +
					"    GROUP BY event_id) AS attendsCount " +
					"    RIGHT JOIN events ON events.id = attendsCount.event_id) " +
					"    JOIN rooms ON events.room_id = rooms.id) " +
					"        JOIN " +
					"    groups ON events.group_id = groups.id ";

			String postSql = "";
			if (groupID != 0 && roomID != 0) {
				postSql = "WHERE rooms.id = " + roomID + " AND groups.id = " + groupID;
			} else if (roomID != 0) {
				postSql = "WHERE rooms.id = " + roomID;
			} else if (groupID != 0) {
				postSql = "WHERE groups.id = " + groupID;
			}
			sql = sql + postSql;

			//sql文を実行するための準備
			PreparedStatement stmt = con.prepareStatement(sql);

			//sql文実行
			ResultSet rs = stmt.executeQuery();

			//rsを一行読み込み
			while (rs.next()) {

				FileOutPutList.add(mapToFileOutPut(rs));
			}
		}
		return FileOutPutList;
	}

	private EventForFileOutPut mapToFileOutPut(ResultSet rs) throws SQLException {
		EventForFileOutPut EventForFileOutPut = new EventForFileOutPut();
		EventForFileOutPut.setTitle(rs.getString("title"));
		EventForFileOutPut.setStart(rs.getTimestamp("start"));
		EventForFileOutPut.setPlaceName(rs.getString("room_name"));
		EventForFileOutPut.setGroupName(rs.getString("group_name"));
		EventForFileOutPut.setNumberOfAttendingUser(rs.getInt("count"));

		return EventForFileOutPut;
	}
}
