package dao;

import java.util.List;

import domain.Attend;

public interface AttendDao {

	//イベントの参加ボタン押下後に追加する
	public void insert(Attend attend) throws Exception;

	//ログインしたユーザが参加するイベントをリスト化する
	public List<Attend> findByLoginId(String loginId) throws Exception;

	//ユーザがイベントに参加しているか否かの判断
	public boolean joining(Attend attend) throws Exception;

	// attendsテーブルからユーザの参加を取り消す
	public void attendDelete(Attend attend) throws Exception;

	//イベントが削除された際にattendsテーブルからevent_idを削除する
	public void attendDeleteEventId(Attend attend) throws Exception;

	//ユーザが削除された際にattendsテーブルからevent_idを削除する
	public void attendDeleteUserId(Attend attend) throws Exception;
}
