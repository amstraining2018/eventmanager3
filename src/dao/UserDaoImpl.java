package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.mindrot.jbcrypt.BCrypt;

import domain.User;
import tools.Constants2;

public class UserDaoImpl implements UserDao {

	private DataSource ds;

	public UserDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	/**
	 * ユーザ一覧用のリストを取ってくる（ユーザID降順）
	 * @param Integer PageCount
	 * ページ数
	 * @return List<User>
	 * ユーザ一覧(5件ずつ)
	 */
	@Override
	public List<User> findAll(Integer PageCount) throws Exception {
		Integer pg = 0;//Limit句は0から数え始めるため初期値は0
		if (PageCount != null) {
			//1ページ目は(0から数えて)1行目スタート、2ページ目は(0から数えて)6行目スタート…
			pg = (PageCount - 1) * 5;
		}
		List<User> userList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT  " +
					"    users.id, users.employee_id, users.login_id, employee.name AS name, groups.name " +
					"FROM " +
					"    users " +
					"        JOIN " +
					"    employee ON users.employee_id = employee.employee_id " +
					"        JOIN " +
					"    groups ON employee.group_id = groups.id " +
					"ORDER BY users.id DESC "//開始日時を降順に並べ替える
					+ " Limit ?, 5";//？に入力された数字から5つ分のデータを取り出す(0からカウントする)
			PreparedStatement stmt = con.prepareStatement(sql);
			//？にはそのページで先頭になる行の全体での番号pgが入る(Integer型に指定)
			stmt.setObject(1, pg, Types.INTEGER);
			//表にあるデータの中で、カーソルが現在の行を指す(初期状態では先頭を指す)
			ResultSet rs = stmt.executeQuery();

			//次にデータがなくなるとfalseを返してループを抜ける
			while (rs.next()) {
				//userListにrsを追加(id,name,groups.name) (1つずつデータを追加している)
				userList.add(mapToUserA(rs));
			}
		}
		//userListのデータ(ユーザの一覧)を持って帰る
		return userList;
	}

	private User mapToUserA(ResultSet rs) throws SQLException {
		User user = new User();
		user.setUserId((Integer) rs.getObject("id"));
		user.setEmployee_id(rs.getString("users.employee_id"));
		user.setName(rs.getString("name"));
		user.setGroupName(rs.getString("groups.name"));
		return user;
	}

	/**
	 * ユーザ登録するときのメソッド
	 * @param User user
	 * 登録項目が入ってる
	 */
	@Override
	public void insert(User user) throws SQLException {
		try (Connection con = ds.getConnection()) {
			String sql = "INSERT INTO " +
					"	users " +
					"(login_id, login_pass, employee_id, type_id, created) " +
					" Values (?,?,?,?,now())";

			PreparedStatement stmt = con.prepareStatement(sql);
			//ここから?の部分の代入処理
			//ユーザーIDをセット
			stmt.setString(1, user.getLogin_id());
			//ユーザーパスワードをセット
			stmt.setString(2, user.getPass());
			//社員IDをセット
			stmt.setString(3, user.getEmployee_id());
			//ユーザー権限
			stmt.setObject(4, user.getTypeId());
			//ここまで
			stmt.executeUpdate();
		}
	}

	/**
	 * ユーザ編集をするときに使うメソッド
	 * @param User user
	 * 編集する項目が入ってる
	 */
	@Override
	public void update(User user) throws Exception {
		//編集管理
		try (Connection con = ds.getConnection()) {
			String sql = "Update users Set users.type_id = ?";
			//ベースになるSQL文

			//パスワードが編集された場合はパスワード用のSQL文を追加
			//|| !user.getPass().isEmpty()
			if (user.getPass() != null) {

				//文字を結合するためのStringBuilderを設定
				StringBuilder buf = new StringBuilder();
				//Sql文をbufに代入
				buf.append(sql);
				//ベースのSql文に↑の文字を結合
				buf.append(", users.login_pass = ? where users.id = ?");
				//結合した結果をsql文に代入
				sql = buf.toString();

				PreparedStatement stmt = con.prepareStatement(sql);
				//stmtで追加した文字の?に代入
				stmt = con.prepareStatement(sql);
				//ユーザー権限をセット
				stmt.setObject(1, user.getTypeId());
				//パスワードをセット
				stmt.setString(2, user.getPass());
				stmt.setObject(3, user.getUserId());
				stmt.executeUpdate();
			} else {
				StringBuilder buf = new StringBuilder();
				//Sql文をbufに代入
				buf.append(sql);
				//ベースのSql文に↑の文字を結合
				buf.append(" where users.id = ?");
				//結合した結果をsql文に代入
				sql = buf.toString();

				PreparedStatement stmt = con.prepareStatement(sql);
				//ユーザー権限をセット
				stmt.setObject(1, user.getTypeId());
				stmt.setObject(2, user.getUserId());
				stmt.executeUpdate();
			}
		}
	}

	/**
	 * ユーザを消すときに使うメソッド
	 * @param int id
	 * 消したい者のユーザID
	 */
	@Override
	public void delete(int id) throws Exception {
		//削除の処理
		try (Connection con = ds.getConnection()) {
			String sql = "DELETE FROM users"
					+ " WHERE id = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			//ユーザーIDをセット
			stmt.setObject(1, id, Types.INTEGER);
			stmt.executeUpdate();
		}
	}

	/**
	 * 	ユーザーIDを通して対象データを見つける
	 * @param userId
	 * @return User
	 */
	@Override
	public User findByUserId(Integer userId) throws Exception {
		User user = new User();
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT  " +
					"    users.id, " +
					"    employee.name, " +
					"    users.login_id, " +
					"    users.login_pass, " +
					"    groups.name, " +
					"    users.type_id " +
					"FROM " +
					"    USERS " +
					"        INNER JOIN " +
					"    employee ON users.employee_id = employee.employee_id " +
					"		JOIN " +
					"	groups ON users.type_id = groups.id " +
					"WHERE " +
					"    USERS.ID = " + userId;
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {
				user.setUserId((Integer) rs.getObject("users.id"));
				user.setName(rs.getString("employee.name"));
				user.setLogin_id(rs.getString("users.login_id"));
				user.setPass(rs.getString("users.login_pass"));
				user.setGroupName(rs.getString("groups.name"));
				user.setTypeId((Integer) rs.getObject("users.type_id"));
			}
		}
		return user;
	}

	/**
	 * ユーザIDで詳細用の情報とってくる
	 * @param user
	 * ユーザID
	 * @return User
	 * ユーザ詳細の情報詰まってる
	 * @author kawamura
	 */
	@Override
	public User userD(Integer user) throws Exception {
		User user2 = null;
		try (Connection conn = ds.getConnection()) {
			String sql = "SELECT  " +
					"    users.id, users.login_id, " +
					"    users.employee_id, " +
					"    employee.name, " +
					"    users.type_id, " +
					"    user_types.name, " +
					"    employee.group_id, " +
					"    groups.name AS group_name " +
					"FROM " +
					"    users " +
					"        JOIN " +
					"    employee ON users.employee_id = employee.Employee_id " +
					"        JOIN " +
					"    groups ON employee.group_id = groups.id " +
					"        JOIN " +
					"    user_types ON users.type_id = user_types.id " +
					"WHERE " +
					"    users.id = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setObject(1, user, Types.INTEGER);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			user2 = mapToUser(rs);
		}
		return user2;
	}

	private User mapToUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setUserId((Integer) rs.getObject("users.id"));
		user.setLogin_id(rs.getString("users.login_id"));
		user.setEmployee_id(rs.getString("users.employee_id"));
		user.setName(rs.getString("name"));
		user.setTypeName(rs.getString("user_types.name"));
		user.setGroupName(rs.getString("group_name"));
		user.setTypeId((Integer) rs.getObject("type_id"));
		return user;
	}

	/**
	 * ページ数を数えるメソッド
	 * @throws Exception
	 */
	@Override
	public Integer countRow() throws Exception {
		Integer rowCount = null;
		try (Connection con = ds.getConnection()) {
			String sql = "select Count(id) as rowCount from users ";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				rowCount = rs.getInt("rowCount");
			}
		}
		return rowCount;
	}

	/**
	 * 受け取ったパスワード文字列をハッシュ化するメソッド
	 * @param pass
	 * String型。登録または変更の際に入力されたユーザのパスワード
	 */
	public String makePasswordHash(String pass) {
		//String型に代入するgetParameterをしたときにこのメソッドを呼んでください
		//受け取ったパスワードをハッシュ化する
		String hashed = BCrypt.hashpw(pass, BCrypt.gensalt());
		return hashed;
	}

	/**
	 * ログインしてるユーザーのパスワード変更するメソッド
	 * @param pass
	 *String型。半角英数で8～12文字の指定。
	 * @param id
	 *Integer型。userIdの値。
	 *  @throws Exception
	 */
	@Override
	public void chanegePassword(String pass, Integer id) throws Exception {
		//編集管理
		try (Connection con = ds.getConnection()) {
			//	パスワードをアップデートするSQL文
			String sql = "UPDATE users SET login_pass = ? WHERE id  =?";
			//stmtで追加した文字の?に代入
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, pass);
			stmt.setObject(2, id, Types.INTEGER);
			stmt.executeUpdate();
		}

	}

	/**
	 * ユーザ一括登録の際に読み込んだcsvファイルのデータが、
	 * テーブルに既存するデータと被っている場合に登録不可となりfalseを返却
	 * @param column
	 * String型 csvファイルの該当する項目名（employeeIdなのかloginIdか）
	 * @param data
	 * String型 csvファイルのデータ行に書かれているemployee_idまたはlogin_id
	 * @return boolean
	 * @throws SQLException
	 */
	@Override
	public boolean checkExsist(String column, String data) throws SQLException {
		boolean ret = false;
		try (Connection con = ds.getConnection()) {
			String sql = null;
			if (column.equals(Constants2.EMPLOYEE)) {
				sql = "SELECT COUNT(*) FROM users WHERE employee_id=?";
			} else if (column.equals(Constants2.EMPLOYEE)) {
				sql = "SELECT COUNT(*) FROM users WHERE login_id=?";
			}
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, data);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			int count = rs.getInt(1);
			if (count == 0) {
				ret = true;
			} else {
				ret = false;
			}
		}
		return ret;
	}

}
